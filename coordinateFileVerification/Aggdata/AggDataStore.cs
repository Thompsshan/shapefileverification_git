﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AggdataShapefileVerificationFramework
{
    public struct AggDataStore
    {
        public List<AggdataList> Lists;
        public Dictionary<string,LinkedList<GeoFile>> Files;
        public Dictionary<string,CountryInfo> PlaceNames;        
    }

    public struct CountryInfo
    {
        public List<string> altNames;
        public Dictionary<string, List<string>> ChildPlaceNames;        
    }
}
