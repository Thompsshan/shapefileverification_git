﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AggdataShapefileVerificationFramework

{
    abstract class ShapeComparer
    {
        //Value from the location you wish to check against the shapefile
        string locationComparison;
        string [] alternateNames= { };
        public ShapeComparer(string locationColumn)
        {
            locationComparison = locationColumn;
        }
        public ShapeComparer(string locationColumn,string [] altNames)
        {
            locationComparison = locationColumn;
            alternateNames = (string []) altNames.Clone();
        }

        //checks if location data is within the shape mentioned inside the shape file
        public bool Compare(AggdataList.Location x, GeoFile y)
        {
            string[] values = new string[alternateNames.Length + 1];
            alternateNames.CopyTo(values, 0);
            values[alternateNames.Length] = x.scrapedInfo[locationComparison];
            return y.checkScrapeColumn(x.lat, x.lng, values);
        }
        //checks against multiple locations
        public bool[] Compare(AggdataList.Location[] x, GeoFile y)
        {
            bool[] results = new bool[x.Length];
            for (int i=0;i<x.Length;i++)
            {
                string[] values = new string[alternateNames.Length + 1];
                alternateNames.CopyTo(values, 0);
                values[alternateNames.Length] = x[i].scrapedInfo[locationComparison];
                results[i] = y.checkScrapeColumn(x[i].lat, x[i].lng, values);
            }
            return results;
        }

        //checks against location and then uses Output Writer to output results
        public bool Compare(AggdataList.Location x, GeoFile y,AggdataOutputWriter writer)
        {
            string[] values = new string[alternateNames.Length + 1];
            alternateNames.CopyTo(values, 0);
            values[alternateNames.Length] = x.scrapedInfo[locationComparison];
            bool result = y.checkScrapeColumn(x.lat, x.lng, values);
            string[] output = { Convert.ToString(result) };
            writer.Output(output);
            return result;
        }

        //checks against multiple locations and then uses Output Writer to output results
        public bool [] Compare(AggdataList.Location[] x, GeoFile y, AggdataOutputWriter writer)
        {
            bool[] results = new bool[x.Length];
            for (int i = 0; i < x.Length; i++)
            {
                string[] values = new string[alternateNames.Length + 1];
                alternateNames.CopyTo(values, 0);
                values[alternateNames.Length] = x[i].scrapedInfo[locationComparison];
                results[i] = y.checkScrapeColumn(x[i].lat, x[i].lng, values);
            }
            //Convert bool to true/false values
            string[] output = new string[results.Length];
            for(int i=0;i<results.Length;i++)
            {
                output[i] = Convert.ToString(results[i]);
            }
            //Output results using OutputWriter
            
            writer.Output(output);
            return results;
            
        }
    }
}
