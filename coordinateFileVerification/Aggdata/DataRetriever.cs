﻿using System;
using System.Collections.Generic;
using MySql.Data.MySqlClient;
using System.Linq;
using System.Text;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Net;
using System.Threading.Tasks;
using System.IO;
using System.Xml.Linq;

namespace AggdataShapefileVerificationFramework
{
    //Retrieves Lists and Locations from a Aggdata resource
    public class DataRetriever
    {
        //Custom Object that can hold all the information retrived for easy(er) access
        public static AggDataStore DataStore;

        //Default retriever using the Aggdata API to retrieve all the lists
        public static List<AggdataList> getAggdataLists()
        {
            Console.WriteLine("Pulling Api Lists...");
            string cookietoken = "token=agg-4p09g51j8";
            string apiURL = "https://api.aggdata.com/list/?limit=5000";

            HttpWebRequest request = WebRequest.Create(apiURL) as HttpWebRequest;
            request.Method = "GET";
            request.Accept = "application / json";
            request.Headers["cookie"] = cookietoken;
            request.Headers["ContentType"] = "charset=utf-8";
            request.Timeout = 5000;
            
            try
            {
                HttpWebResponse response = request.GetResponse() as HttpWebResponse;
                WebHeaderCollection header = response.Headers;



                //var encoding = ASCIIEncoding.ASCII;
                string responseText = "";
                var encoding = Encoding.UTF8;
                using (var reader = new System.IO.StreamReader(response.GetResponseStream(),encoding))
                {
                    responseText = reader.ReadLine();
                    responseText = responseText.Replace("short_name", "name").Replace("payload_id", "payloadId").Replace("parent_id", "parentId").Replace("\"parentId\":null","\"parentId\":0");

                    var settings = new JsonSerializerSettings
                    {
                        NullValueHandling = NullValueHandling.Ignore,
                        MissingMemberHandling = MissingMemberHandling.Ignore
                    };
                    var apiLists = JsonConvert.DeserializeObject<List<AggdataList>>(responseText,settings);

                    //makes a deep copy of the list in datastore, theoretically duplicating changes to Datastore, requires testing
                    DataStore.Lists = apiLists;
                    return apiLists;

                }
            }catch (WebException webex)
            {
                var info = new StreamReader(webex.Response.GetResponseStream()).ReadToEnd();
                Console.WriteLine("Error:" + webex.Message);
                return null;
            }
            
            
           
            
           // response.Headers["ContentType"] = "text/csv";
           
            
        }

        public static List<AggdataList> getAggdataLists(string datestring)
        {
            Console.WriteLine("Pulling Api Lists...");
            string cookietoken = "token=agg-4p09g51j8";
            string apiURL = "https://api.aggdata.com/list/locations/?fromdate="+datestring;

            HttpWebRequest request = WebRequest.Create(apiURL) as HttpWebRequest;
            request.Method = "GET";
            request.Accept = "application / json";
            request.Headers["ContentType"] = "charset=utf-8";
            request.Headers["cookie"] = cookietoken;
            request.Timeout = 5000;

            try
            {
                HttpWebResponse response = request.GetResponse() as HttpWebResponse;
                WebHeaderCollection header = response.Headers;



                
                string responseText = "";
                var encoding = Encoding.UTF8;
                using (var reader = new System.IO.StreamReader(response.GetResponseStream(),encoding))
                {
                    responseText = reader.ReadLine();
                    responseText = responseText.Replace("short_name", "name").Replace("payload_id", "payloadId").Replace("parent_id", "parentId").Replace("\"parentId\":null", "\"parentId\":0");

                    var settings = new JsonSerializerSettings
                    {
                        NullValueHandling = NullValueHandling.Ignore,
                        MissingMemberHandling = MissingMemberHandling.Ignore
                    };

                    JToken[] listArr = JArray.Parse(responseText).Children().ToArray();
                    List<AggdataList> apiLists = new List<AggdataList>();
                    foreach (JToken list in listArr)
                    {
                        var apiList = list.Children().Where(token => token.Path.Contains("list_id")).First().Value<JToken>();
                        var id=apiList.Children().First().SelectToken("id");
                        var name=apiList.Children().First().SelectToken("name");
                        var parent= apiList.Children().First().SelectToken("parentId");
                        var date = list.Children().Where(token => token.Path.Contains("date_published")).First().Children().First().Value<string>();
                        

                        AggdataList newList = new AggdataList(id.Value<int>(),name.Value<string>() );
                        newList.parentId = parent.Value<int>();
                        newList.published_date = date;            
                        apiLists.Add(newList);
                    }
                    

                    //makes a deep copy of the list in datastore, theoretically duplicating changes to Datastore, requires testing
                    DataStore.Lists = apiLists;
                    return apiLists;

                }
            }
            catch (WebException webex)
            {
                var info = new StreamReader(webex.Response.GetResponseStream()).ReadToEnd();
                Console.WriteLine("Error:" + webex.Message);
                return null;
            }




            // response.Headers["ContentType"] = "text/csv";


        }



        public static List<AggdataList.Location> getListLocations(AggdataList list)
        {
           // Console.WriteLine("Pulling Locations from List # {0}...", list.id);
            string cookietoken = "token=agg-4p09g51j8";
            string apiURL = "https://api.Aggdata.com/list/locations/" + list.id+"?fields=ali,archived_location_id,latitude,longitude,city,state,county,country,zip_code,address,address_line_2,address_line_3,geo_accuracy";
            HttpWebRequest request = WebRequest.Create(apiURL) as HttpWebRequest;
            request.Timeout = 50000;
            request.Method = "GET";
            request.Accept = "application / json";
            request.Headers["Cookie"] = cookietoken;
            
            HttpWebResponse response = request.GetResponse() as HttpWebResponse;
            response.Headers["ContentType"] = "charset=utf-8";
            WebHeaderCollection header = response.Headers;

            var encoding = Encoding.UTF8;
            string responseText = "";

            using (var reader = new System.IO.StreamReader(response.GetResponseStream(), encoding))
            {
                responseText = reader.ReadLine();
                if (responseText == null )
                {
                    return null;
                }
                responseText=responseText.Replace("latitude", "lat").Replace("longitude", "lng").TrimStart('[').TrimEnd(']').Replace("\"lat\":null","\"lat\":0.0").Replace("\"lng\":null", "\"lng\":0.0");
                if ( responseText == "")
                {
                    return null;
                }
                
                if(JObject.Parse(responseText).Children().Where(token => token.Path == "versions").First().Values().Count() == 0)
                {
                    return null;
                }
                JToken [] locationArray = JObject.Parse(responseText).Children().Where(token=> token.Path=="versions").First().Values().First().SelectToken("locations").ToArray();
                List<AggdataList.Location> apiLocations = new List<AggdataList.Location>();
                foreach(JToken location in locationArray)
                {
                    if (location.SelectToken("country").Value<string>()==null || location.SelectToken("state").Value<string>() == null
                        || location.SelectToken("lat").Value<double>()==0.0 || location.SelectToken("lng").Value<double>() == 0.0
                        )
                    {
                        continue;
                    }
                    AggdataList.Location locationObject = location.ToObject<AggdataList.Location>();
                    locationObject.county = locationObject.county.ToLower().Replace(" county", "").Replace(" parish","");
                    locationObject.appendScrapeInfo("county",locationObject.county);
                    //change all values to lowercase and append to scraped info for variable access (ie location.scrapedinfo[columnname] vs using a bunch of reflection nonsense
                    locationObject.state = locationObject.state.ToLower();
                    locationObject.appendScrapeInfo("state", locationObject.state);
                    locationObject.country = locationObject.country.ToLower();
                    locationObject.appendScrapeInfo("country", locationObject.country);
                    locationObject.zip_code = locationObject.zip_code.ToLower().Replace(" ", "");
                    locationObject.appendScrapeInfo("zip_code", locationObject.zip_code);
                    locationObject.city = locationObject.city.ToLower().Replace(" ", "");
                    locationObject.appendScrapeInfo("city", locationObject.city);
                    
                    

                    //append the rest to scraped info
                    locationObject.appendScrapeInfo("address", locationObject.county);
                    locationObject.appendScrapeInfo("address_line_2", locationObject.county);
                    locationObject.appendScrapeInfo("address_line_3", locationObject.county);
                    locationObject.appendScrapeInfo("ali", locationObject.ali);

                    apiLocations.Add(locationObject);
                }

                

                return apiLocations;
            }
        }

        //Otherwise can use the Premium database
        /*public static List<AggdataList> getAggdataLists(mySQLUser sqluser)
        {
            List<AggdataList> Lists = new List<AggdataList>();
            sqluser.userConnection.Open();
            string sqlCode = "SELECT lists.id,short_name,payloads.archived_location_count,parent_id FROM lists join payloads ON lists.id=payloads.list_id AND payloads.id IN " +
                "(Select pl.id "
                + "  From payloads pl Join (Select _pl.list_id As list_id, "
                + "     Max(_pl.scraped_at) As scraped_at "
                + "    From payloads _pl "
                + "    Where _pl.status = 'published' "
                + "    Group By _pl.list_id) x On pl.list_id = x.list_id And pl.scraped_at = "
                + "      x.scraped_at "
                + "  Where pl.status = 'published') ORDER BY lists.id";
            MySqlCommand query = new MySqlCommand(sqlCode, sqluser.userConnection);
            MySqlDataReader reader = query.ExecuteReader();
            while (reader.Read())
            {
                //adds List info to lists
                AggdataList newList = new AggdataList((int)reader.GetValue(0), (string)reader.GetValue(1));
                newList.size = (int)reader.GetValue(2);
                //parent id alsways equals parent ID or its own ID if it is the parent/ has no children
                if (reader.GetValue(4) == null)
                {
                    newList.parentId = (int)reader.GetValue(0);
                }
                else
                {
                    newList.parentId = (int)reader.GetValue(4);
                }
                Lists.Add(newList);
            }
            reader.Close();
            sqluser.userConnection.Close();
            DataStore.Lists = Lists;
            return Lists;
        }

        public static List<AggdataList.Location> getListLocations(mySQLUser sqluser,AggdataList list)
        {
            //currently no way for extra columns, can be added later.
            List<AggdataList.Location> Locations = new List<AggdataList.Location>();
            sqluser.userConnection.Open();
            string sqlCode = "SELECT al.id,latitude,longitude,country,state,city,zip_code,county,address,address_line_2,address_line_3 FROM archived_locations al" +
                             "join payloads ON al.list_id = payloads.list_id AND payloads.id IN" +
                             "(Select pl.id From payloads pl Join (Select _pl.list_id As list_id, Max(_pl.scraped_at) As scraped_at" +
                             "From payloads _pl" +
                             "Where _pl.status = 'published'" +
                             "Group By _pl.list_id) x On pl.list_id = x.list_id And pl.scraped_at =" +
                             "x.scraped_at" +
                             "Where pl.status = 'published') AND al.list_id =" + list.id;
            MySqlCommand query = new MySqlCommand(sqlCode, sqluser.userConnection);
            MySqlDataReader reader = query.ExecuteReader();
            while (reader.Read())
            {
                AggdataList.Location newLocation = new AggdataList.Location((int)reader.GetValue(0), (int)reader.GetValue(1), (int)reader.GetValue(2), list);
                for (int i = 3; i < reader.FieldCount; i++)
                {
                    string columnValue;
                    if (reader.GetValue(i) == System.DBNull.Value)
                    {
                        columnValue = "";
                    }
                    else
                    {
                        columnValue = (string)reader.GetValue(i);
                    }
                    newLocation.appendScrapeInfo(reader.GetName(i), columnValue.ToLower());
                }
                Locations.Add(newLocation);
            }     

            return Locations;
        }
        */

        public static string [] getAlternateLocationNames(string country,string state)
        {
            //Checks if Datastore exists and if not, creates it
            
            if (DataStore.PlaceNames==null)
            {
                //Console.WriteLine("new placenames");
                DataStore.PlaceNames = new Dictionary<string, CountryInfo>();
            }

            //Remove extranious spaces and characters and make case insensitive
            if (country == null)
            {
                return new string[] { state };
            }
            country = country.Replace(".", "").Replace("-","").ToLower();
            state = state.Replace(" ", "").Replace(".", "").Replace("-", "").ToLower();

            //Console.WriteLine("Getting Alternate names for " + country);
            //Null Conditions
            if (country == "")
            {
                return new string[] { state };
            }
            else if (state == "")
            {
                //Check if country exists
                //if exists, return just the country values
                if (DataStore.PlaceNames.ContainsKey(country))
                {
                    return DataStore.PlaceNames[country].altNames.ToArray();
                }
                //else continue and find country values
            }

            
            //Check if DataStore.PlaceNames has existing info Country Abbreviation
            if (DataStore.PlaceNames.ContainsKey(country))
            {
                //Country Found check if there is info on state
               // Console.WriteLine("Country Stored..Pulling Values for " + country);

                //State not found, Returning whatever Country info can be retrieved and original state data
                if (!DataStore.PlaceNames[country].ChildPlaceNames.ContainsKey(state))
                {
                    //Console.WriteLine("Country doesnt have state");
                    string[] allNames=new string [DataStore.PlaceNames[country].altNames.Count+2];
                    allNames[0] = country;
                    allNames[1] = state;
                    DataStore.PlaceNames[country].altNames.CopyTo(allNames,2);
                    return allNames;
                }
                

                //State is found and all info is returned
                //Console.WriteLine("State Stored..Pulling Values for "+ state);
                string[] returnNames = new string[DataStore.PlaceNames[country].altNames.Count + DataStore.PlaceNames[country].ChildPlaceNames[state].Count()+2];
                returnNames[0] = country;
                returnNames[DataStore.PlaceNames[country].altNames.Count] = state;
                DataStore.PlaceNames[country].altNames.CopyTo(returnNames, 1);
                DataStore.PlaceNames[country].ChildPlaceNames[state].CopyTo(returnNames, DataStore.PlaceNames[country].altNames.Count + 1);
                return returnNames;
            }
            else
            {
                string countryCode = country;
                string countryName;
                //get info from geonames API
                //Console.WriteLine("Pulling GeonameInfo");

                //check response is status message for overuse

                //check if results are more than 0, else get infomation on that country name
                XDocument xml = XDocument.Parse(getGeonames(countryCode));
                
                //If no results are returned, check if the country is an alternate name, and get info for that name to recall with proper abbreviation
                if (xml.Descendants("geoname").Count() == 0)
                {
                    //request information on country name and get abbreviation
                    //Console.WriteLine("Country Info may not in abbreviated form, getting abbreviation");
                    string countryURL = "http://api.geonames.org/search?username=shanet&featureCode=PCLD&featureCode=PCLI&orderBy=relevance&name_equals=" + WebUtility.UrlEncode(country);

                    HttpWebRequest request2 = WebRequest.Create(countryURL) as HttpWebRequest;
                    System.Net.ServicePointManager.ServerCertificateValidationCallback = (senderX, certificate, chain, sslPolicyErrors) => { return true; };
                    HttpWebResponse response2 = request2.GetResponse() as HttpWebResponse;
                    response2.Headers["ContentType"] = "text/xml";
                    Stream stream2 = response2.GetResponseStream();
                    StreamReader reader2 = new StreamReader(stream2, Encoding.UTF8);
                    string xmlstring2 = reader2.ReadToEnd();
                    String preamble = Encoding.UTF8.GetString(Encoding.UTF8.GetPreamble());
                    if (xmlstring2.StartsWith(preamble))
                    {
                        var lastIndexOfUtf8 = preamble.Length - 1;
                        xmlstring2 = xmlstring2.Remove(0, lastIndexOfUtf8);
                    }
                    XDocument xml2 = XDocument.Parse(xmlstring2);


                    //if result is still empty, return only given name and nothing else can be done
                    if (xml2.Descendants("geoname").Count() == 0)
                    {
                       // Console.WriteLine("Couldn't find supplimental information");
                        return new string[] { country, state };
                    }
                    
                    //retrieve first result
                    XElement node = xml2.Descendants("geoname").First();
                    //Console.WriteLine("Country CodH:" + node.Descendants("countryCode").First().Value.ToLower());

                    //set countryCode to actual country code
                    countryCode = node.Descendants("countryCode").First().Value.ToLower();

                    if (DataStore.PlaceNames.ContainsKey(node.Descendants("countryCode").First().Value.ToLower()))
                    {
                        //Country is an alternate name, return results with Country code
                        //if country isn't stored as an alternate name, add it to alternate names
                        if (!DataStore.PlaceNames[node.Descendants("countryCode").First().Value.ToLower()].altNames.Contains(country))
                        {
                            DataStore.PlaceNames[node.Descendants("countryCode").First().Value.ToLower()].altNames.Add(country);
                        }
                        return getAlternateLocationNames(node.Descendants("countryCode").First().Value.ToLower(),state);
                    }
                    
                    
                    xml = XDocument.Parse(getGeonames(node.Descendants("countryCode").First().Value.ToLower()));

                }
                //else parse info recieved
                if (xml.FirstNode.Document.Descendants("totalResultsCount").First().Value == "0")
                {
                    return null;
                }
                //add country to PlaceNames
                countryName = xml.Descendants("countryName").First().Value.ToLower();
                CountryInfo info = new CountryInfo();
                if (info.altNames == null)
                {
                    info.altNames = new List<string>();
                }
                info.altNames.Add(countryName);
                info.ChildPlaceNames = new Dictionary<string, List<string>>();
                /*if(xml.Descendants("countryName").First().Value.ToLower() != xml.Descendants("toponymName").First().Value.ToLower())
                {
                    info.altNames.Add(xml.Descendants("toponymName").First().Value.ToLower());
                }*/
                DataStore.PlaceNames.Add(countryCode, info);

                foreach (XElement node in xml.Descendants("geoname"))
                {
                    string stateabbr;

                    //Use official ISO code if available, otherwise use AdminCode's value
                    if (node.Descendants("adminCode1").First().Attribute("ISO3166-2") != null)
                    {
                        stateabbr = node.Descendants("adminCode1").First().Attribute("ISO3166-2").Value.ToLower();
                    }
                    else
                    {
                        stateabbr = node.Descendants("adminCode1").First().Value.ToLower();
                    }
                    /*Console.WriteLine("Country Info:" + node.Descendants("countryName").First().Value.ToLower());
                    Console.WriteLine("Country CodH:" + node.Descendants("countryCode").First().Value.ToLower());
                    Console.WriteLine("State Abbrev:" + stateabbr);
                    Console.WriteLine("State NamH:" + node.Descendants("adminName1").First().Value.ToLower());
                    Console.WriteLine("Toponym NamH:" + node.Descendants("toponymName").First().Value.ToLower());*/


                    //checks if Datastore has Placenames for country
                    if (!DataStore.PlaceNames.ContainsKey(node.Descendants("countryCode").First().Value.ToLower()))
                    {
                        CountryInfo cinfo = new CountryInfo();
                        cinfo.altNames = new List<string>();
                        cinfo.altNames.Add(countryName);
                        cinfo.ChildPlaceNames = new Dictionary<string, List<string>>();

                        DataStore.PlaceNames.Add(node.Descendants("countryCode").First().Value.ToLower(), cinfo);
                        
                    }

                    if (!DataStore.PlaceNames[node.Descendants("countryCode").First().Value.ToLower()].ChildPlaceNames.ContainsKey(stateabbr))
                    {
                        List<string> stateAlts = new List<string>();
                        stateAlts.Add(node.Descendants("adminName1").First().Value.ToLower());
                        if(node.Descendants("adminName1").First().Value.ToLower() != node.Descendants("toponymName").First().Value.ToLower())
                        {
                            stateAlts.Add(node.Descendants("toponymName").First().Value.ToLower());
                        }
                        DataStore.PlaceNames[node.Descendants("countryCode").First().Value.ToLower()].ChildPlaceNames.Add(stateabbr,stateAlts);
                    }
                }

                if (!DataStore.PlaceNames[countryCode].ChildPlaceNames.ContainsKey(state.ToLower()))
                {
                    //Console.WriteLine("State is unofficial or doesn't exist");
                    string[] allnames = new string[DataStore.PlaceNames[countryCode].altNames.Count+2];
                    allnames[0] = country;
                    DataStore.PlaceNames[countryCode].altNames.CopyTo(allnames, 1);
                    allnames[DataStore.PlaceNames[countryCode].altNames.Count() + 1] = state;
                    return allnames;
                }
                string[] returnNames = new string[DataStore.PlaceNames[countryCode].altNames.Count+ DataStore.PlaceNames[countryCode].ChildPlaceNames[state].Count+2];
                returnNames[0] = country;
                DataStore.PlaceNames[countryCode].altNames.CopyTo(returnNames, 1);
                returnNames[DataStore.PlaceNames[countryCode].altNames.Count + 1] = state;
                DataStore.PlaceNames[countryCode].ChildPlaceNames[state].CopyTo(returnNames, DataStore.PlaceNames[countryCode].altNames.Count + 2);
                return returnNames;
            }

        }

        public static string getGeonames(string countryAbbrev)
        {
            string apiURL = "http://api.geonames.org/search?username=shanet&style=full&countryBias=US&featureCode=ADM1&country=" + countryAbbrev;
            try
            {
                HttpWebRequest request = WebRequest.Create(apiURL) as HttpWebRequest;
                System.Net.ServicePointManager.ServerCertificateValidationCallback = (senderX, certificate, chain, sslPolicyErrors) => { return true; };
                HttpWebResponse response = request.GetResponse() as HttpWebResponse;
                response.Headers["ContentType"] = "text/xml";
                Stream stream = response.GetResponseStream();
                String preamble = Encoding.UTF8.GetString(Encoding.UTF8.GetPreamble());
                StreamReader reader = new StreamReader(stream, Encoding.UTF8);
                string xmlstring = reader.ReadToEnd();
                if (xmlstring.StartsWith(preamble))
                {
                    var lastIndexOfUtf8 = preamble.Length - 1;
                    xmlstring = xmlstring.Remove(0, lastIndexOfUtf8);
                }
                return xmlstring;
            }
            
            catch (TimeoutException e)
            {
                return "";
            }
        }

        public static bool checkInAlternateNames(string altName)
        {
            if (DataStore.PlaceNames == null)
            {
                DataStore.PlaceNames = new Dictionary<string, CountryInfo>();
                return false;
            }
            foreach (CountryInfo info in DataStore.PlaceNames.Values)
            {
                if (info.altNames.Contains(altName))
                {
                    return true;
                }
            }
            return false;
        }

        public static LinkedList<GeoFile> getShapefiles(AggdataList.Location location, string currentDir)
        {
            if (DataStore.Files == null)
            {
                DataStore.Files = new Dictionary<string, LinkedList<GeoFile>>();
                LinkedList<GeoFile> defaultFiles = new LinkedList<GeoFile>();
                
                foreach (string path in Directory.GetFiles(currentDir))
                {
                    if (Path.GetExtension(path) == ".shp" || Path.GetExtension(path) == ".kml")
                    {




                        var countryFile = GeoFile.parseFileURL(path);
                        countryFile.comparison = "country";
                        countryFile.ReadPolygons();
                        defaultFiles.AddFirst(countryFile);

                    }
                }
                

                DataStore.Files.Add("default", defaultFiles);
            }
            
            //check if files exist in Datastore
            if (DataStore.Files.ContainsKey(location.country))
            {
                return DataStore.Files[location.country];
            }
            else if (location.country !="canada" && location.country !="united states" && location.country != "unitedstates" && location.country != "us" && location.country != "US")
            {
                //location
               return DataStore.Files["default"];
            }
            
            LinkedList<GeoFile> shapeFiles = new LinkedList<GeoFile>();

            string[] files = Directory.GetFiles(currentDir);
            foreach (string path in files)
            {
                if (Path.GetExtension(path) == ".shp" || Path.GetExtension(path) == ".kml")
                {



                    
                    var shape = GeoFile.parseFileURL(path);
                    shape.ReadPolygons();
                    shape.comparison = "country";
                    LinkedListNode<GeoFile> node = new LinkedListNode<GeoFile>(shape);
                    shapeFiles.AddFirst(shape);
                    
                }
            }
            currentDir += "\\" + location.country.ToLower().Replace(" ", "");
            if (!Directory.Exists(currentDir))
            {
                //Console.WriteLine("No more shape files found for this country");
                DataStore.Files.Add(location.country, shapeFiles);
                return shapeFiles;
            }
           
            while (Directory.GetDirectories(currentDir).Length == 1)
            {
                files = Directory.GetFiles(currentDir);
                foreach (string path in files)
                {
                    if (Path.GetExtension(path) == ".shp" || Path.GetExtension(path) == ".kml")
                    {
                        
                        var shape = GeoFile.parseFileURL(path);
                        if (Path.GetFileName(currentDir) == location.country.ToLower().Replace(" ", ""))
                        {
                            shape.comparison = "state";
                        }
                        else
                        {
                            shape.comparison = Path.GetFileName(currentDir);
                        }
                        shape.ReadPolygons();
                        
                        shapeFiles.AddLast(shape);
                    }
                }
                currentDir = Directory.GetDirectories(currentDir)[0];
            }
            
            files = Directory.GetFiles(currentDir);
            foreach (string path in files)
            {
                if (Path.GetExtension(path) == ".shp" || Path.GetExtension(path) == ".kml")
                {
                    var shape = GeoFile.parseFileURL(path);
                    shape.comparison = shape.comparison = Path.GetFileName(currentDir);
                    shape.ReadPolygons();
                    LinkedListNode<GeoFile> node = new LinkedListNode<GeoFile>(shape);
                    shapeFiles.AddLast(shape);
                }
            }
            DataStore.Files.Add(location.country, shapeFiles);
            return shapeFiles;
        }

        public class mySQLUser
        {
            string user;
            string pass;
            string server;
            string database;
            private string connectionstring;
            public MySqlConnection userConnection=null;

            public mySQLUser(string username,string password,string server,string database)
            {
                user = username;
                pass = password;
                this.server = server;
                this.database = database;
                connectionstring =
                "server=" + server + ";" +
                "database=" + database + ";" +
                "user=" + user + ";" +
                "password=" + pass + ";";
                
                //if credentials are invalid, return error message
                try
                {
                    userConnection=new MySqlConnection(connectionstring);
                }
                catch (MySqlException e)
                {
                    Console.WriteLine("An Error Occurred with the Database connection");
                    Console.WriteLine(e.Message);
                }
                
            }

            
            
        }
    }
}
