﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Newtonsoft.Json;
using System.Threading.Tasks;

namespace AggdataShapefileVerificationFramework
{
    public class AggdataList
    {
        public int id;
        public string name;
        public string published_date;
        public int payloadId;
        public int size;
        public int parentId;
        public List<Location> Locations;
        public List<string> columnsUsed;


       /* public AggdataList(int id, string name, int payload, int size)
        {
            this.id = id;
            this.name = name;
            payloadId = payload; //maybe not necessary
            this.size = size;
            Locations = new List<Location>();
        }*/
        public  AggdataList (int id, string name)
        {
            this.id = id;
            this.name = name;
            Locations = new List<Location>();
        }
        
        public Location AddLocation(int id, double lat, double lng, AggdataList list)
        {
            Location location = new Location(id, lat, lng, list);

            Locations.Add(location);
            return location;
        }
        public Location GetLocation(int i)
        {
            return Locations.ElementAt(i);
        }
        public List<AggdataList.Location> GetLocations()
        {
            return Locations;
        }
        //inner class for locations
        public class Location
        {
            public int id;
            public double lat;
            public double lng;
            public string city;
            public string country;
            public string county;
            public string state;
            public string zip_code;
            public string geo_accuracy;
            public string ali;
            public string address;
            public string address_line_2;
            public string address_line_3;

            public Dictionary<string, string> scrapedInfo;
            public AggdataList list;

            public Location(int id, double lat, double lng, AggdataList outerList)
            {
                this.id = id;
                this.lat = lat;
                this.lng = lng;
                scrapedInfo = new Dictionary<string, string>();
                this.list = outerList;

            }
            public void appendScrapeInfo(string columnName, string value)
            {
                if (!scrapedInfo.ContainsKey(columnName))
                {
                    // Console.WriteLine(value);
                    scrapedInfo.Add(columnName, value);
                }

            }
        }
    }
}
