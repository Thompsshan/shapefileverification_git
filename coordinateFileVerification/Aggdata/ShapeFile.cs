﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Diagnostics;
using DotSpatial.Data;
using DotSpatial.Projections;
using ClipperLib;
using DotSpatial.Topology;
using System.Text.RegularExpressions;
using System.Linq;

namespace AggdataShapefileVerificationFramework
{
    public class ShapeFile : GeoFile
    {
        List<IFeature> Features;
        public IFeatureSet featureSet;
        int nameIndex;



        public ShapeFile(String filepath, string comparison, double buffer)
        {
            this.filepath = filepath.Replace(@"/", @"//");
            this.comparison = comparison;



        }
        public ShapeFile(String filepath, string comparison)
        {
            this.filepath = filepath.Replace(@"/", @"//");
            this.comparison = comparison;



        }
        public ShapeFile(String filepath)
        {
            this.filepath = filepath.Replace(@"/", @"//");
            this.comparison = "";



        }
        public override string GetComparisonColumn()
        {
            return comparison;
        }
        public override void ReadPolygons()
        {


            string[] prjfiles = Directory.GetFiles(Path.GetDirectoryName(filepath), "*.prj");
            //no projection information, assume default (WGS84)
            if (prjfiles.Length == 0)
            {
                featureSet = FeatureSet.Open(filepath);

                return;
            }
            string prjFilepath = prjfiles[0];
            string projectionString;
            using (StreamReader r = new StreamReader(prjFilepath))
            {
                projectionString = r.ReadToEnd();
            }


            featureSet = FeatureSet.Open(filepath); ;
            featureSet.Projection.ParseEsriString(ProjectionInfo.Open(prjFilepath).ToEsriString());
            //change projection system to WGS84 (a standard projection) if it does not use lat long
            if (featureSet.Projection.IsLatLon == false)
            {

                ProjectionInfo latLongProj = KnownCoordinateSystems.Geographic.World.WGS1984;
                featureSet.Reproject(latLongProj);
            }









        }

        public override string[] findContainingPolygons(double lat, double lng)
        {

            Coordinate coord = new Coordinate(lng, lat);
            Point point = new Point(coord);

            IEnumerator<IFeature> enumerator = featureSet.Features.GetEnumerator();
            List<string> foundPolies = new List<string>();
            while (enumerator.MoveNext())
            {
                Polygon item = enumerator.Current.BasicGeometry as Polygon;
                if (item == null)
                {
                    //must be a multipolygon
                    MultiPolygon item2 = enumerator.Current.BasicGeometry as MultiPolygon;
                    try
                    {
                        if (item2.Contains(point))
                        {
                            string name = "No Name";
                            name = enumerator.Current.DataRow.ItemArray[this.fileIndex].ToString();
                            foundPolies.Add(name);

                        }
                    }
                    catch (DotSpatial.Topology.TopologyException e)
                    {
                        continue;
                    }
                }
                else
                {
                    try
                    {
                        if (item.Contains(point))
                        {

                            string name = "No Name";
                            name = enumerator.Current.DataRow.ItemArray[fileIndex].ToString();
                            foundPolies.Add(name);



                        }
                    }
                    catch (DotSpatial.Topology.TopologyException e)
                    {
                        continue;
                    }

                }
            }
            return foundPolies.ToArray();
        }
        public override string GetFilepath()
        {
            return this.filepath;
        }

        private Feature bufferShape(Feature shape)
        {
            //double buffer = 0.1;
            Polygon feature = shape.BasicGeometry as Polygon;
            if (buffer == 0)
            {
                return shape;
            }
            return (Feature)shape.Buffer(buffer);

            //stuff to use Clipper, Clipper had issues so I went back to using the Native Buffer Function
            if (feature == null)
            {
                MultiPolygon feature2 = shape.BasicGeometry as MultiPolygon;


                List<List<IntPoint>> polies = new List<List<IntPoint>>();
                foreach (Polygon poly in feature2.Geometries)
                {
                    List<IntPoint> clipperCoords = new List<IntPoint>();

                    foreach (Coordinate coordinate in poly.Coordinates)
                    {
                        IntPoint point = new IntPoint((long)(coordinate.X * 1000000000.00), (long)(coordinate.Y * 1000000000.00));
                        clipperCoords.Add(point);
                    }
                    // clipperCoords.Add(new IntPoint((long)(poly.Coordinates.First().X * 100000.00), (long)(poly.Coordinates.First().Y * 100000.00)));


                    polies.Add(clipperCoords);
                }
                List<Coordinate> outputCoords = new List<Coordinate>();
                var buffed = Clipper.OffsetPolygons(polies, 1, JoinType.jtMiter, 0, true);
                if (buffed.Count() == 0)
                {
                    return null;
                }
                var buffEnum = buffed.GetEnumerator();
                List<Polygon> outPolies = new List<Polygon>();
                while (buffEnum.MoveNext())
                {
                    var buffedShape = buffEnum.Current;
                    for (int i = 0; i < buffedShape.Count; i++)
                    {
                        outputCoords.Add(new Coordinate(buffedShape.ElementAt(i).X / 1000000000.00, buffedShape.ElementAt(i).Y / 1000000000.00));
                    }
                    // outputCoords.Add(new Coordinate(buffedShape.ElementAt(0).X / 100000.00, buffedShape.ElementAt(0).Y / 100000.00));
                    outPolies.Add(new Polygon(new LinearRing(outputCoords)));
                }


                Feature outShape = new Feature();
                outShape = shape.Copy();
                outShape.BasicGeometry = new MultiPolygon(outPolies.ToArray());

                FeatureSet set = new FeatureSet();
                set.AddFeature(outShape);
                set.SaveAs("E://CoordinateFileVerification//output.shp", true);
                return outShape;
            }
            else
            {
                List<IntPoint> clipperCoords = new List<IntPoint>();
                foreach (Coordinate coordinate in feature.Coordinates)
                {
                    IntPoint point = new IntPoint((long)(coordinate.X * 100000.00), (long)(coordinate.Y * 100000.00));
                    clipperCoords.Add(point);
                }
                List<List<IntPoint>> polies = new List<List<IntPoint>>();
                List<Coordinate> outputCoords = new List<Coordinate>();
                polies.Add(clipperCoords);
                var buffed = Clipper.OffsetPolygons(polies, buffer * 100000.00, JoinType.jtMiter, 0, true).First();
                for (int i = 0; i < buffed.Count(); i++)
                {
                    Coordinate tempCoord = new Coordinate((double)buffed.ElementAt(i).X / 100000.00, (double)buffed.ElementAt(i).Y / 100000.00);
                    outputCoords.Add(tempCoord);
                }
                Feature outShape = new Feature();


                outShape = shape.Copy();
                /*List<Coordinate> newCoords = new List<Coordinate>();
                foreach (Coordinate item in outputCoords)
                {
                    outShape.BasicGeometry.Coordinates.Add(item);
                }*/
                outShape.BasicGeometry = new Polygon(new LinearRing(outputCoords));
                //Console.Write(outShape.BasicGeometry.Coordinates);

                FeatureSet outputSet = new FeatureSet();
                outputSet.AddFeature(outShape.BasicGeometry);
                outputSet.SaveAs("E://CoordinateFileVerification//output.shp", true);
                return outShape;
            }


        }

        override public bool checkScrapeColumn(double lat, double lng, string[] values)
        {

            Coordinate coord = new Coordinate(lng, lat);
            Point point = new Point(coord);

            foreach (string name in values)
            {

                var reducedShapes = featureSet.Features;
                foreach (var shape in reducedShapes)
                {

                    if (shape.DataRow.ItemArray[fileIndex].ToString().ToLower().Replace(".", "").Replace(" ", "") == name.ToLower().Replace(".", "").Replace(" ", ""))
                    {

                        Polygon poly = shape.BasicGeometry as Polygon;
                        if (poly == null)
                        {
                            MultiPolygon poly2 = shape.BasicGeometry as MultiPolygon;
                            if (poly2.Contains(point))
                            {

                                return true;
                            }
                            else if (((MultiPolygon)bufferShape((Feature)shape).BasicGeometry).Contains(point))
                            {
                                return true;
                            }
                        }
                        else
                        {
                            if (poly.Contains(point))
                            {

                                return true;
                            }
                            else if (((Polygon)bufferShape((Feature)shape).BasicGeometry).Contains(point))
                            {
                                return true;
                            }
                        }
                    }
                }
            }

            return false;
        }

        protected T reduceShape<T>(T shape)
        {
            throw new NotImplementedException();
        }

        public override Shape[] GetPolygon(string shapename)
        {
            // var reducedShapes = featureSet.Features;
            List<Shape> matchShapes = new List<Shape>();
            foreach (var feature in featureSet.Features)
            {

                if (feature.DataRow.ItemArray[this.fileIndex].ToString().ToLower() == shapename.ToLower())
                {
                    Shape newShape = new Shape();
                    Polygon poly = feature.BasicGeometry as Polygon;
                    if (poly == null)
                    {
                        MultiPolygon poly2 = feature.BasicGeometry as MultiPolygon;
                        newShape.Polygon = poly2;
                        newShape.name = feature.DataRow.ItemArray[this.fileIndex].ToString().ToLower().Replace(".", "").Replace(" ", "");
                    }
                    else
                    {
                        newShape.Polygon = poly;
                        newShape.name = feature.DataRow.ItemArray[this.fileIndex].ToString().ToLower().Replace(".", "").Replace(" ", "");
                    }
                    matchShapes.Add(newShape);
                }

            }
            return matchShapes.ToArray();
        }

        public override Shape[] GetPolygon(string[] names)
        {
            List<Shape> matchShapes = new List<Shape>();
            List<string> shapeNames = names.CloneList();

            foreach (var feature in featureSet.Features)
            {

                if (shapeNames.Contains(feature.DataRow.ItemArray[this.fileIndex].ToString().ToLower()))
                {
                    Shape newShape = new Shape();
                    Polygon poly = feature.BasicGeometry as Polygon;
                    if (poly == null)
                    {
                        MultiPolygon poly2 = feature.BasicGeometry as MultiPolygon;
                        newShape.Polygon = poly2;
                        newShape.name = feature.DataRow.ItemArray[this.fileIndex].ToString().ToLower().Replace(".", "").Replace(" ", "");
                    }
                    else
                    {
                        newShape.Polygon = poly;
                        newShape.name = feature.DataRow.ItemArray[this.fileIndex].ToString().ToLower().Replace(".", "").Replace(" ", "");
                    }
                    matchShapes.Add(newShape);
                }

            }


            return matchShapes.ToArray();
        }

        public override bool checkPolygonForPoint(GeoFile.Shape poly, double latitude, double longitude)
        {

            Coordinate coord = new Coordinate(longitude, latitude);
            Point point = new Point(coord);

            IEnumerator<IFeature> enumerator = featureSet.Features.GetEnumerator();
            if (poly.Polygon.Contains(point))
            {
                return true;

            }
            return false;

        }

        public override string getPolygonInfo(Shape shapeObject)
        {

            return shapeObject.name;
        }


        public override string[] findContainingBoundingBoxes(double latitude, double longitude)
        {
            Coordinate coord = new Coordinate(longitude, latitude);
            Point point = new Point(coord);

            IEnumerator<IFeature> enumerator = featureSet.Features.GetEnumerator();
            List<string> foundPolies = new List<string>();

            while (enumerator.MoveNext())
            {
                if (enumerator.Current.Envelope.Contains(coord))
                {
                    string name = "No Name";

                    //check Geonames for the name, if it exists, use that
                    name = enumerator.Current.DataRow.ItemArray[this.fileIndex].ToString();

                    foundPolies.Add(name);
                }
            }
            var polies = findContainingPolygons(latitude, longitude);
            return foundPolies.ToArray();

        }

        public override void outputReducedShapefile(string filename)
        {
            throw new NotImplementedException();
        }
    }
}
