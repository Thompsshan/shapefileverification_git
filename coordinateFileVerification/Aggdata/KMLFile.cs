﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Text.RegularExpressions;
using SharpKml.Engine;
using SharpKml.Dom;
using SharpKml.Base;
using ClipperLib;



namespace AggdataShapefileVerificationFramework
{

    //Possible class to use to sort shapes by east to west, possibly improving efficiency by skipping shapes less than given longitude;

    //KML class for comparison
    public class KMLFile : GeoFile
    {
        //create a list sorted by west boundry, you can check if lat is more west than that coordinate, and if so, it can be eliminated 
        List<Polygon> Polygons;




        public KMLFile(String filepath, String comparison)
        {
            this.filepath = filepath;
            this.comparison = comparison;            
            Polygons = new List<Polygon>();
           // Console.WriteLine("Contruct KML File:" + filepath);

            TextReader reader = new StreamReader(filepath);
            KmlFile file = KmlFile.Load(reader);

            Kml kml = file.Root as Kml;
            foreach (var item in kml.Flatten().OfType<Placemark>())
            {
                //Console.WriteLine(item.Name);
                foreach (Polygon polygon in item.Flatten().OfType<SharpKml.Dom.Polygon>())
                {
                    
                    Polygons.Add(polygon);
                }
            }

        }
        public KMLFile(String filepath, String comparison, double buffer)
        {
            this.filepath = filepath;
            this.comparison = comparison;
            this.buffer = buffer;
            Polygons = new List<Polygon>();
           // Console.WriteLine("Contruct KML File:" + filepath);

            TextReader reader = new StreamReader(filepath);
            KmlFile file = KmlFile.Load(reader);

            Kml kml = file.Root as Kml;
            foreach (var item in kml.Flatten().OfType<Placemark>())
            {
                //Console.WriteLine(item.Name);
                foreach (Polygon polygon in item.Flatten().OfType<SharpKml.Dom.Polygon>())
                {
                    Polygons.Add(polygon);
                }
            }
        }
        public KMLFile(String filepath)
        {
            this.filepath = filepath;
            this.comparison = "";
            this.buffer = 0.0;
            Polygons = new List<Polygon>();
           // Console.WriteLine("Contruct KML File:" + filepath);

            TextReader reader = new StreamReader(filepath);



            
            KmlFile file = KmlFile.Load(reader);

            Kml kml = file.Root as Kml;
            foreach (var item in kml.Flatten().OfType<Placemark>())
            {
                //Console.WriteLine(item.Name);
                foreach (Polygon polygon in item.Flatten().OfType<SharpKml.Dom.Polygon>())
                {
                    Polygons.Add(polygon);
                }
            }
        }


        public override void ReadPolygons()
        {


        }
        public override string GetComparisonColumn()
        {
            return comparison;
        }

        public override bool checkPolygonForPoint(Shape poly, double latitude, double longitude)
        {
            SharpKml.Base.Vector coordinates = new SharpKml.Base.Vector();
            coordinates.Latitude = latitude;
            coordinates.Longitude = longitude;


            Polygon polygon = poly.Polygon;
            
            CoordinateCollection points = polygon.OuterBoundary.LinearRing.Coordinates;
            Vector endPoint = points.Last<Vector>();
            double lastLng = endPoint.Longitude;
            double lastLat = endPoint.Latitude;
            bool isInside = false;


            //check a bounding box first
            if (!polygon.CalculateBounds().Contains(coordinates.Latitude, coordinates.Longitude))
            {
                return false;
            }

            int coordinateCount = polygon.OuterBoundary.LinearRing.Coordinates.Count;
            double startLng;
            double startLat;
            for (int i = 0; i < coordinateCount; i++)
            {
                
                startLat = lastLat;
                startLng = lastLng;
                endPoint =polygon.OuterBoundary.LinearRing.Coordinates.ElementAt(i);

                lastLat = endPoint.Latitude;
                lastLng = endPoint.Longitude;

                isInside ^= (lastLat > coordinates.Latitude ^ startLat > coordinates.Latitude) &&
                    ((coordinates.Longitude - lastLng) < (((coordinates.Latitude - lastLat) * (startLng - lastLng)) / (startLat - lastLat)));
               
            }

            return isInside;

        }

        override public bool checkScrapeColumn(double lat, double lng, string[] values)
        {
            
            foreach (string name in values)
            {
              var selectedList = Polygons.Where(poly => poly.GetParent<Placemark>().Name.Replace("<at>", "").Replace("<openparen>", "").Replace("<closeparen>", "").Replace(" ", "").Replace(".", "").ToLower()== name.ToLower().Replace(".", "").Replace(" ", "")) ;
                if (selectedList.Count() > 0)
                {
                    foreach (Polygon poly in selectedList)
                    {
                        GeoFile.Shape abstractShape = new Shape();
                        abstractShape.Polygon = poly;
                        if (checkPolygonForPoint(abstractShape, lat, lng))
                        {
                            return true;
                        }
                        abstractShape.Polygon = bufferShape(poly);
                        //check again with the buffered shape
                        if (checkPolygonForPoint(abstractShape, lat, lng))
                        {
                            return true;
                        }

                    }  
                    //for zipcodes and FSA's when close enough is fine
                }else if (this.comparison=="zip_code")
                {
                    selectedList = Polygons.Where(poly => name.ToLower().Replace(" ", "").Replace("-", "").Contains(poly.GetParent<Placemark>().Name.Replace("<at>", "").Replace("<openparen>", "").Replace("<closeparen>", "").Replace(" ", "").Replace(".", "").ToLower()));
                    foreach (Polygon poly in selectedList)
                    {
                        GeoFile.Shape abstractShape = new Shape();
                        abstractShape.Polygon = poly;
                        if (checkPolygonForPoint(abstractShape, lat, lng))
                        {
                            return true;
                        }
                        abstractShape.Polygon = bufferShape(poly);
                        //check again with the buffered shape
                        if (checkPolygonForPoint(abstractShape, lat, lng))
                        {
                            return true;
                        }

                    }

                }  
                
            }
            return false;
        }

        override public string[] findContainingPolygons(double latitude, double longitude)
        {
            SharpKml.Dom.Polygon[] polygons = Polygons.ToArray();
            List<string> foundPolies = new List<string>();

            for (int i = 0; i < polygons.Length; i++)
            {
                GeoFile.Shape abstractShape = new Shape();
                abstractShape.Polygon = polygons[i];
                if (checkPolygonForPoint(abstractShape, latitude, longitude))
                {

                    Placemark poly = polygons[i].GetParent<Placemark>();


                    foundPolies.Add(poly.Name.Replace("<at>", "").Replace("<openparen>", "").Replace("<closeparen>", "").ToLower());
                }
            }
            return foundPolies.ToArray();
        }
        public override string GetFilepath()
        {
            return this.filepath;
        }

        private Polygon bufferShape(Polygon shape)
        {
            CoordinateCollection coords = shape.OuterBoundary.LinearRing.Coordinates;
            List<Vector> coordList = coords.ToList<Vector>();
            List<IntPoint> clipperCoords = new List<IntPoint>();
            List<Vector> outputCoords = new List<Vector>();
            foreach (Vector coordinate in coordList)
            {
                long latitude = (long)(coordinate.Latitude * 100000);
                long longitude = (long)(coordinate.Longitude * 100000);
                IntPoint point = new IntPoint(longitude,latitude);

                clipperCoords.Add(point);
            }
            List<List<IntPoint>> polygons = new List<List<IntPoint>>();
            polygons.Add(clipperCoords);
            var buffed = Clipper.OffsetPolygons(polygons, buffer * 100000, JoinType.jtMiter, 0, true).First();
            for (int i = 0; i < buffed.Count(); i++)
            {
                Vector tempVect = new Vector((double)buffed.ElementAt(i).Y / 100000, (double)buffed.ElementAt(i).X / 100000);
                outputCoords.Add(tempVect);
            }
            Polygon outShape = new Polygon();
            try
            {
                LinearRing ring = new LinearRing();
                ring.Coordinates = new CoordinateCollection(outputCoords);
                outShape.OuterBoundary = new OuterBoundary();
                outShape.OuterBoundary.LinearRing = ring;
            }catch(OutOfMemoryException e)
            {
                Console.Write("out of memory");
            }
            // outShape = shape.Clone();
            //outShape.OuterBoundary.LinearRing.Coordinates = new CoordinateCollection(outputCoords);

            /*MultipleGeometry testout = new MultipleGeometry();
            
            testout.AddGeometry(outShape);
            outputShape(testout, "C:\\Users\\shanet\\Desktop\\");*/

            return outShape;
            
        }
        protected SharpKml.Dom.MultipleGeometry reduceShape(Polygon shape)
        {
            double tolerance = 0.01;
            

            CoordinateCollection coords = shape.OuterBoundary.LinearRing.Coordinates;
            List<Vector> coordList = coords.ToList<Vector>();
            int length = coordList.Count;

            double accuumLat = 0;
            double accuumLng = 0;
            double avgLat = 0;
            double avgLng = 0;
            bool isInsideDistance = false;
            int numCoordinatesAveraged = 1;

            List<Vector> smallCoords = new List<Vector>();
            Vector selected = coordList.First();
            
            accuumLat = selected.Latitude;
            accuumLng = selected.Longitude;
            

            Stack<Vector> recentPoints = new Stack<Vector>();

            
            recentPoints.Push(coordList.ElementAt(0));
            recentPoints.Push(coordList.ElementAt(1));
            recentPoints.Push(coordList.ElementAt(2));
             recentPoints.Push(coordList.ElementAt(3));
            /*if (length > 10)
            {
                recentPoints.Push(coordList.ElementAt(4));
                recentPoints.Push(coordList.ElementAt(5));
                recentPoints.Push(coordList.ElementAt(6));
                recentPoints.Push(coordList.ElementAt(7));
                recentPoints.Push(coordList.ElementAt(8));
                recentPoints.Push(coordList.ElementAt(9));                
            }
            */
            //smallCoords.Add(coordList.First());
            //coordList.Add(coordList.First());


            List<IntPoint> clipperCoords = new List<IntPoint>();
            for (int i = 0; i < coordList.Count(); i++)
            {
                IntPoint point = new IntPoint((long)(coordList.ElementAt(i).Longitude*100000),(long)(coordList.ElementAt(i).Latitude*100000));
                
                clipperCoords.Add(point);
            }
            List<List<IntPoint>> polygons = new List<List<IntPoint>>();
            polygons.Add(clipperCoords);
            //var buffed = Clipper.CleanPolygon(clipperCoords,buffer);
            
            var buffed =Clipper.OffsetPolygons(polygons,buffer*100000, JoinType.jtMiter,0,true).First();
                        
            for (int i = 0; i < buffed.Count(); i++)
            {
                Vector tempVect = new Vector((double)buffed.ElementAt(i).Y/100000, (double)buffed.ElementAt(i).X/100000);
                smallCoords.Add(tempVect);
               
            }



            /*
            for (int i = 1; i < length+1; i++)
            {
                

                isInsideDistance = false;
                
                double distance = Math.Sqrt((Math.Pow((selected.Latitude - coordList.ElementAt(i).Latitude), 2) + Math.Pow((selected.Longitude - coordList.ElementAt(i).Longitude), 2)));
                //if outside max distance, add next point to accumulators
                List<Vector> coordInDistance = recentPoints.Where<Vector>(point => Math.Sqrt((Math.Pow((point.Latitude - coordList.ElementAt(i).Latitude), 2) + Math.Pow((point.Longitude - coordList.ElementAt(i).Longitude), 2))) <= tolerance).ToList();
             /*   if (coordInDistance.Count() > 0)                
                {
                    
                    isInsideDistance = true;
                    accuumLat += coordList.ElementAt(i).Latitude;
                    accuumLng += coordList.ElementAt(i).Longitude;
                    numCoordinatesAveraged++;

                    


                }

                if (!isInsideDistance)
                {
                    //if inside requirements, get average (if 1 than it is standard coordinate) and append it to new set
                    avgLat = accuumLat / numCoordinatesAveraged;
                    avgLng = accuumLng / numCoordinatesAveraged;
                    
                    

                    //Vector prevPoint = smallCoords.Last();
                    Vector prevPoint = new Vector(recentPoints.Average(point => point.Latitude), recentPoints.Average(point => point.Longitude));
                    double widthDistance = avgLat - prevPoint.Latitude;
                    double heightDistance =avgLng - prevPoint.Longitude;
                    double totalDistance = widthDistance + heightDistance;

                    //the angle between the previous point and the buffered next point
                    double e = Math.Atan(buffer / totalDistance);
                    
                    //the angle between the buffered point and the current unbuffered point
                    double f = Math.Atan(totalDistance / buffer);
                    double xdisplace = totalDistance * Math.Sin(e);
                    double ydisplace = buffer * Math.Cos(f);


                    
                     if (avgLat-prevPoint.Latitude < 0)
                     {
                        
                            avgLat -= xdisplace;
                        




                    }
                     else if(avgLat - prevPoint.Latitude > 0)
                     {
                        
                            avgLat += xdisplace;
                       
                            
                        
                    }
                     
                     

                    if (avgLng - prevPoint.Longitude < 0)
                    {
                        
                            avgLng += ydisplace;
                        
                            
                        
                        
                    }
                    else if (avgLng - prevPoint.Longitude > 0)
                    {
                        
                            avgLng -=  ydisplace;

                        
                        
                    }

                        Vector[] bufferedLine = {new Vector(smallCoords.Last().Latitude, smallCoords.Last().Longitude), new Vector(avgLat, avgLng) };
                         Vector[] originalLine = {new Vector(coordList.ElementAt(i-1).Latitude,coordList.ElementAt(i-1).Longitude),new Vector(coordList.ElementAt(i).Latitude,coordList.ElementAt(i).Longitude) };

                         double delta= ((bufferedLine[0].Longitude-bufferedLine[1].Longitude)*(originalLine[0].Latitude-originalLine[1].Latitude))-((originalLine[0].Longitude-originalLine[1].Longitude)*(bufferedLine[0].Latitude-bufferedLine[1].Latitude))    ;
                    if (delta> 0)
                    {

                        if (avgLat - prevPoint.Latitude < 0)
                        {

                            avgLat += xdisplace;





                        }
                        else if (avgLat - prevPoint.Latitude > 0)
                        {

                            avgLat -= xdisplace;



                        }



                        if (avgLng - prevPoint.Longitude < 0)
                        {

                            avgLng -= ydisplace;




                        }
                        else if (avgLng - prevPoint.Longitude > 0)
                        {

                            avgLng += ydisplace;



                        }

                    }

                    if (delta <= 0)
                    {
                        if (avgLat - prevPoint.Latitude < 0)
                        {

                            avgLat -= xdisplace;





                        }
                        else if (avgLat - prevPoint.Latitude > 0)
                        {

                            avgLat += xdisplace;



                        }



                        if (avgLng - prevPoint.Longitude < 0)
                        {

                            avgLng += ydisplace;




                        }
                        else if (avgLng - prevPoint.Longitude > 0)
                        {

                            avgLng -= ydisplace;



                        }
                    }


                        /* Shape shapeOb = new Shape();
                         shapeOb.Polygon = shape;
                         if (checkPolygonForPoint(shapeOb, avgLat, avgLng))
                         {

                         }
                         




                        numCoordinatesAveraged = 1;
                    Vector avgcoordinate = new Vector();
                    avgcoordinate.Latitude = avgLat;
                    avgcoordinate.Longitude = avgLng;
                    smallCoords.Add(avgcoordinate);

                    selected = coordList.ElementAt(i);
                    accuumLat = selected.Latitude;
                    accuumLng = selected.Longitude;
                    if (recentPoints.Count > 0)
                    {
                        recentPoints.Pop();
                        recentPoints.Push(avgcoordinate);
                    }
                }

                //Append for total Lat

                


            }
            */
            Polygon smallPoly = new SharpKml.Dom.Polygon();
            smallPoly.OuterBoundary = new OuterBoundary();
            smallPoly.OuterBoundary.LinearRing = new LinearRing();
            smallPoly.OuterBoundary.LinearRing.Coordinates = new CoordinateCollection();
            foreach(Vector point in smallCoords)
            {
                smallPoly.OuterBoundary.LinearRing.Coordinates.Add(point);
            }



            MultipleGeometry newShape = new MultipleGeometry();
            newShape.AddGeometry(smallPoly);


            //newShape.AddGeometry(smallPoly);
            Polygon boxShape = new Polygon();
            BoundingBox bound = shape.CalculateBounds();
            List<Vector> boundPoints = new List<Vector>();
            boundPoints.Add(new Vector(bound.North,bound.West));
            boundPoints.Add(new Vector(bound.South, bound.West));
            boundPoints.Add(new Vector(bound.South, bound.East));
            boundPoints.Add(new Vector(bound.North, bound.East));
            boundPoints.Add(new Vector(bound.North, bound.West));

            boxShape.OuterBoundary = new OuterBoundary();
            boxShape.OuterBoundary.LinearRing = new LinearRing();
            boxShape.OuterBoundary.LinearRing.Coordinates = new CoordinateCollection(boundPoints);
            //newShape.AddGeometry(boxShape);
            
           
            return newShape;
            

        }


        public override void outputReducedShapefile(string filename)
        {
            Document kmltest = new Document();

            kmltest.Name = filename;
            kmltest.Open = false;
            
            foreach (Polygon poly in Polygons)
            {
                

                Placemark reduced;
                List<Placemark> places = kmltest.Flatten().OfType<Placemark>().ToList();

                if (places.Where(place => place.Name == poly.GetParent<Placemark>().Name.Replace("<at>", "").Replace("<openparen>", "").Replace("<closeparen>", "")).ToList().Count > 0)
                {

                    reduced = kmltest.Flatten().OfType<Placemark>().Where(place => place.Name == poly.GetParent<Placemark>().Name.Replace("<at>", "").Replace("<openparen>", "").Replace("<closeparen>", "")).First();
                }
                else
                {
                    reduced = new Placemark();
                    Style style = new Style();
                    LineStyle line = new LineStyle();
                    PolygonStyle polygon = new PolygonStyle();
                    line.Width = 2;
                    line.Color = Color32.Parse("ff000000");
                    polygon.Color = Color32.Parse("77FF88cc");
                    polygon.Fill = true;
                    style.Line = line;
                    style.Polygon = polygon;
                    reduced.AddStyle(style);

                    reduced.Name = poly.GetParent<Placemark>().Name.Replace("<at>", "").Replace("<openparen>", "").Replace("<closeparen>", "");
                }



                MultipleGeometry multi;
                if (reduced.Geometry == null)
                {
                    multi = new MultipleGeometry();
                    if (reduceShape(poly) == null)
                    {
                        //Dont add empty shapes
                        continue;
                    }
                    if (reduceShape(poly).Geometry.OfType<Polygon>().First().OuterBoundary.LinearRing.Coordinates.Count > 0)
                    {
                        multi.AddGeometry(reduceShape(poly).Geometry.OfType<Polygon>().First().Clone());
                    }
                    else
                    {

                    }
                   
                    reduced.Geometry = multi;
                    kmltest.AddFeature(reduced);
                }
                else
                {
                   
                    MultipleGeometry current = (MultipleGeometry)reduced.Geometry;
                    current.AddGeometry(reduceShape(poly));
                }
            }

            //Output KML for testing



            var KMLDoc = new Kml();
            
            KMLDoc.Feature = kmltest;
            KmlFile kmlfile = KmlFile.Create(KMLDoc, true);

            kmlfile.Save(File.Create("E:\\CoordinateFileVerification\\"+filename+".kml"));

        }

        private void outputShape(MultipleGeometry shape,string outputDirectory)
        {
            var KMLDoc = new Kml();
            Document kmlout = new Document();
            kmlout.Name = outputDirectory+"bufferedShape"+".kml";
            kmlout.Open = false;

            Placemark place = new Placemark();
            Style style = new Style();
            LineStyle line = new LineStyle();
            PolygonStyle polygon = new PolygonStyle();
            line.Width = 2;
            line.Color = Color32.Parse("ff000000");
            polygon.Color = Color32.Parse("77FF88cc");
            polygon.Fill = true;
            style.Line = line;
            style.Polygon = polygon;
            place.AddStyle(style);

            //place.Name = shape.GetParent<Placemark>().Name.Replace("<at>", "").Replace("<openparen>", "").Replace("<closeparen>", "");

            place.Geometry = shape;
            kmlout.AddFeature(place);
            

            KMLDoc.Feature = kmlout;
            KmlFile kmlfile = KmlFile.Create(KMLDoc, true);

            kmlfile.Save(File.Create(outputDirectory + "buffered.kml"));
            
        }
        

        public override Shape[] GetPolygon(string name)
        {
            
            Polygon[] polies = Polygons.Where<Polygon>(shape=> shape.GetParent<Placemark>().Name.Replace("<at>", "").Replace("<openparen>", "").Replace("<closeparen>", "").ToLower() == name).ToArray<Polygon>();
            List<GeoFile.Shape> retlist = new List<Shape>();
            foreach(Polygon poly in polies)
            {
                Shape shape = new Shape();
                shape.setShape(poly);
                //Console.WriteLine(shape.Polygon.OuterBoundary.LinearRing.Coordinates.Count);
                retlist.Add(shape);
            }
            return retlist.ToArray();
        }



        public override Shape[] GetPolygon(string[] names)
        {
            List<Shape> shapes = new List<Shape>();
            foreach(string name in names)
            {
                List<Polygon> polies =this.Polygons.Where<Polygon>(shape => shape.GetParent<Placemark>().Name.Replace("<at>", "").Replace("<openparen>", "").Replace("<closeparen>", "").Replace(" ", "").Replace(".", "").ToLower() == name).ToList();
                foreach(Polygon poly in polies)
                {
                    Shape shape = new Shape();
                    shape.setShape<Polygon>(poly);
                    Polygon realShape = shape.Polygon;
                    Console.WriteLine(realShape.OuterBoundary.LinearRing.Coordinates.First().Latitude);
                    shapes.Add(shape);
                }                
            }
            return shapes.ToArray<Shape>();
        }

        public override string getPolygonInfo(GeoFile.Shape shape)
        {
            var poly = (SharpKml.Dom.Polygon)shape.Polygon;
            string values =  poly.GetParent<Placemark>().Name.Replace("<at>", "").Replace("<openparen>", "").Replace("<closeparen>", "").Replace(" ", "").Replace(".", "").ToLower() ;
            return values;
        }


        public override string[] findContainingBoundingBoxes(double latitude, double longitude)
        {
            SharpKml.Dom.Polygon[] polygons = Polygons.ToArray();
            List<string> foundBoxes = new List<string>();

            for (int i = 0; i < polygons.Length; i++)
            {

                if (polygons[i].CalculateBounds().Contains(latitude, longitude))
                {

                    Placemark poly = polygons[i].GetParent<Placemark>();


                    foundBoxes.Add(poly.Name.Replace("<at>", "").Replace("<openparen>", "").Replace("<closeparen>", "").Replace(" ","").Replace(".","").ToLower());
                }
            }
            return foundBoxes.ToArray();
        }
    }
}

