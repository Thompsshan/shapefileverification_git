﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AggdataShapefileVerificationFramework
{
    class ShapefileFinder
    {
        public static string[] findShape(AggdataList.Location location,GeoFile shapefile)
        {
            //same as below but for only 1 location
            return shapefile.findContainingPolygons(location.lat, location.lng);            
        }
        public static string[] findShape(AggdataList.Location[] locations, GeoFile shapefile)
        {
            List<string> results = new List<string>();
            //check each location against the shapefile
            for(int i = 0; i < locations.Length; i++)
            {
                results.AddRange(shapefile.findContainingPolygons(locations[i].lat, locations[i].lng));
            }
            //set result to the index of the array
            return results.ToArray();
        }
        public static void findShape(AggdataList.Location[] locations,GeoFile shapefile, AggdataOutputWriter writer)
        {
            //read locations and get list of results
            string [] results = findShape(locations, shapefile);
            writer.Output(results);
        }
    }
}
