﻿using System;
using System.Collections.Generic;
using System.IO;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Linq;
using System.Text;



namespace AggdataShapefileVerificationFramework

{
    public abstract class GeoFile
    {
        
        
        public class Shape
        {
            
            public dynamic Polygon;
            public string name;
            public void setShape<T>(T polygon)
            {
                Polygon = polygon;         
                
            }
            
        }

        public double buffer;
        public int fileIndex;
        public string filepath;
        public string condition;
        public string comparison;
        public int[] listIds;
        


        
        abstract public void  ReadPolygons();
        abstract public string[] findContainingPolygons(double lat, double lng);
        abstract public string[] findContainingBoundingBoxes(double lat, double lng);
        abstract public bool checkPolygonForPoint(Shape Polygon, double lat, double lng);
        abstract public string GetComparisonColumn();
        abstract public string GetFilepath();
        abstract public bool checkScrapeColumn(double lat, double lng, string[] scrapedInfo);
        abstract public string getPolygonInfo(Shape shapeObject);
        abstract public GeoFile.Shape[] GetPolygon (string shapename);
        abstract public GeoFile.Shape[] GetPolygon(string[] names);
        abstract public void outputReducedShapefile(string filename);
        
        

        public void setFileIndex(int index)
        {
            this.fileIndex = index;
        }
        public void setCondition(string cond)
        {
            this.condition = cond;
        }
        public void setListIds(int [] ids)
        {
            listIds = (int[])ids.Clone();
        }

        public static GeoFile parseFileURL(string url)
        {
            if (Path.GetExtension(url).Equals(".kml"))
            {
                //Console.WriteLine("Processing " + url + " as KML file");
                KMLFile kml = new KMLFile(url);

                string[] files = Directory.GetFiles(Path.GetDirectoryName(url));
                files = files.Where(x => x.Contains("info.json")).ToArray();
                var info = files.First();
                StreamReader text = new StreamReader(info);
                JsonTextReader reader = new JsonTextReader(text);
                JObject json = JObject.Load(reader);                
                var buffer = json.SelectToken("buffer");
                kml.buffer = buffer.Value<double>();

                return kml;
            }
            else if (Path.GetExtension(url).Equals(".shp"))
            {
                //Console.WriteLine("Processing " + url + " as Shape file");
                ShapeFile newObject = new ShapeFile(url);
                string [] files =Directory.GetFiles(Path.GetDirectoryName(url));
                files=files.Where(x => x.Contains("info.json")).ToArray();
                var info = files.First();
                StreamReader text = new StreamReader(info);
                JsonTextReader reader = new JsonTextReader(text);
                
                
                JObject json = JObject.Load(reader);
                var index =json.SelectToken("nameIndex");
                newObject.fileIndex = index.Value<int>();
                var buffer = json.SelectToken("buffer");
                newObject.buffer = buffer.Value<double>();

                return newObject;


            }
            else
            {
                throw new InvalidFileTypeException();
            }
            
            
            
        }

        
        
    }

    public class InvalidFileTypeException : Exception
    {
        public InvalidFileTypeException() : base("File Type is Currently Unsupported, only .Shp and .Kml files are allowed")
        {
            
        }
    }
}
