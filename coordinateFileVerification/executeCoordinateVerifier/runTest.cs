﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Diagnostics;
using System.Text;
using System.Net;
using Newtonsoft.Json;
using AggdataShapefileVerificationFramework;



namespace coordinateFileVerification
{
    class Program
    {
        private static int id;

        static void Main(string[] args)
        {
            //have list of shapefiles to use here, iterating through each file
            Console.WriteLine("Start Coordinates Check..");
            Console.WriteLine("Root ProcessId=={0}", Process.GetCurrentProcess().Id);
            /*if (args.Length % 2 != 0)
            {
                Console.WriteLine("Command Line Arguements must be in the form of exe [filename] [column-name]");
                System.Environment.Exit(0);
            }*/

            int numFiles = (int)Math.Floor((decimal)args.Length / 2);

            List<string> inputpaths = new List<string>();
            List<string> columnames = new List<string>();
            List<double> buffers = new List<double>();
            List<string> sqlConditions = new List<string>();
            List<int[]> listIds = new List<int[]>();

            bool hasbuffer = false;
            bool hasSQLCondition = false;
            bool hasListIds = false;

            for (int i = 0; i < args.Length; i++)
            {


                switch (args[i])
                {
                    case "-b":
                        hasbuffer = true;
                        Console.WriteLine("Has Buffer");
                        //i++;
                        break;

                    case "-c":
                        Console.WriteLine("Has Sql");
                        hasSQLCondition = true;
                        //i++;
                        break;
                    case "-l":
                        Console.WriteLine("Has List ids");
                        hasListIds = true;
                        break;
                    default:

                        inputpaths.Add(args[i]);

                        columnames.Add(args[i + 1]);

                        i++;
                        if (hasbuffer && hasSQLCondition && hasListIds)
                        {
                            buffers.Add(double.Parse(args[i + 1]));
                            Console.WriteLine(args[i + 1]);
                            sqlConditions.Add(args[i + 2]);


                            List<int> idArray = new List<int>();
                            foreach (string listId in args[i + 3].Split(','))
                            {
                                if (!int.TryParse(listId, out id))
                                {
                                    Console.WriteLine(id + " is not a valid list id");
                                }
                                else
                                {

                                    idArray.Add(id);
                                }
                            }
                            listIds.Add(idArray.ToArray());

                            Console.WriteLine(args[i + 2]);
                            Console.WriteLine(args[i + 3]);
                            i += 3;
                            hasSQLCondition = false;
                            hasbuffer = false;
                            hasListIds = false;

                        }
                        else if (hasbuffer && hasSQLCondition)
                        {
                            buffers.Add(double.Parse(args[i + 1]));
                            Console.WriteLine(args[i + 1]);
                            sqlConditions.Add(args[i + 2]);
                            Console.WriteLine(args[i + 2]);
                            i += 2;
                            hasSQLCondition = false;
                            hasbuffer = false;
                            int[] idArray = { 0 };
                            listIds.Add(idArray);
                        }
                        else if (hasbuffer && hasListIds)
                        {
                            buffers.Add(double.Parse(args[i + 1]));
                            Console.WriteLine(args[i + 1]);
                            List<int> idArray = new List<int>();
                            foreach (string listId in args[i + 2].Split(','))
                            {
                                if (!int.TryParse(listId, out id))
                                {
                                    Console.WriteLine(id + " is not a valid list id");
                                }
                                else
                                {

                                    idArray.Add(id);
                                }
                            }
                            listIds.Add(idArray.ToArray());
                            Console.WriteLine(args[i + 2]);
                            sqlConditions.Add("");
                            i += 2;
                            hasListIds = false;
                            hasbuffer = false;

                        }
                        else if (hasListIds && hasSQLCondition)
                        {

                            sqlConditions.Add(args[i + 1]);
                            Console.WriteLine(args[i + 1]);
                            List<int> idArray = new List<int>();
                            foreach (string listId in args[i + 2].Split(','))
                            {
                                if (!int.TryParse(listId, out id))
                                {
                                    Console.WriteLine(id + " is not a valid list id");
                                }
                                else
                                {

                                    idArray.Add(id);
                                }
                            }
                            listIds.Add(idArray.ToArray());
                            Console.WriteLine(args[i + 2]);
                            i += 2;
                            buffers.Add(0.0);

                            hasSQLCondition = false;
                            hasListIds = false;

                        }
                        else if (hasListIds)
                        {
                            List<int> idArray = new List<int>();
                            foreach (string listId in args[i + 1].Split(','))
                            {
                                if (!int.TryParse(listId, out id))
                                {
                                    Console.WriteLine(id + " is not a valid list id");
                                }
                                else
                                {
                                    idArray.Add(id);
                                }
                            }
                            listIds.Add(idArray.ToArray());
                            Console.WriteLine(args[i + 1]);
                            i += 1;
                            hasListIds = false;
                            buffers.Add(0.0);
                            sqlConditions.Add("");

                        }
                        else if (hasbuffer)
                        {
                            Console.WriteLine("buffer:" + args[i + 1]);
                            buffers.Add(double.Parse(args[i + 1]));
                            sqlConditions.Add("");
                            int[] idArray = { 0 };
                            listIds.Add(idArray);
                            i++;
                            hasbuffer = false;
                        }
                        else if (hasSQLCondition)
                        {
                            sqlConditions.Add(args[i + 1]);
                            buffers.Add(0.0);
                            int[] idArray = { 0 };
                            listIds.Add(idArray);
                            Console.WriteLine(args[i]);
                            i++;
                            hasSQLCondition = false;
                        }
                        else
                        {
                            buffers.Add(0.0);
                            sqlConditions.Add("");
                            int[] idArray = { 0 };
                            listIds.Add(idArray);

                        }
                        continue;

                }

            }

            
            runCoordinatesCheck checkCoords = new runCoordinatesCheck(inputpaths.ToArray(), columnames.ToArray(), sqlConditions.ToArray(), buffers.ToArray(), listIds.ToArray<int[]>());
        }
    }
}

