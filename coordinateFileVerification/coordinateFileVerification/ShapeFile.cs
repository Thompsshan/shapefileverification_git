﻿using System;
using System.Collections.Generic;
using System.IO;
using DotSpatial.Data;
using DotSpatial.Projections;
using DotSpatial.Topology;
using System.Text.RegularExpressions;




namespace coordinateFileVerification
{
    class ShapeFile : GeoFile
    {
        List<IFeature> Features;
        public IFeatureSet featureSet;
        private String filepath;
        double buffer;
        public string comparison;

        public ShapeFile(String filepath, string comparison, double buffer)
        {
            this.filepath = filepath.Replace(@"/", @"//");
            this.comparison = comparison;
            this.buffer = buffer;
            Console.WriteLine("Contruct Shape File:" + filepath);

        }
        public ShapeFile(String filepath, string comparison)
        {
            this.filepath = filepath.Replace(@"/", @"//");
            this.comparison = comparison;
            this.buffer = 0.0;
            Console.WriteLine("Contruct Shape File:" + filepath);

        }
        public override string GetComparisonColumn()
        {
            return comparison;
        }
        public override void ReadPolygons()
        {


            string[] prjfiles = Directory.GetFiles(Path.GetDirectoryName(filepath), "*.prj");

            string prjFilepath = prjfiles[0];
            string projectionString;
            using (StreamReader r = new StreamReader(prjFilepath))
            {
                projectionString = r.ReadToEnd();
            }
            featureSet = FeatureSet.Open(filepath);
            featureSet.Projection.ParseEsriString(ProjectionInfo.Open(prjFilepath).ToEsriString());

        }

        public override string findContainingPolygon(double lat, double lng)
        {
            Coordinate coord = new Coordinate(lng, lat);
            Point point = new Point(coord);

            IEnumerator<IFeature> enumerator = featureSet.Features.GetEnumerator();

            while (enumerator.MoveNext())
            {
                Polygon item = enumerator.Current.BasicGeometry as Polygon;
                if (item == null)
                {
                    //must be a multipolygon
                    MultiPolygon item2 = enumerator.Current.BasicGeometry as MultiPolygon;
                    if (item2.Contains(point))
                    {

                        string name = "No Name";
                        //for (int i = 0; i < item.DataRow.ItemArray.Length; i++){
                        for (int i = 0; i < enumerator.Current.DataRow.ItemArray.Length; i++)
                        {
                            //if (Regex.IsMatch(item.DataRow.ItemArray[i].ToString(), @"^([a-zA-Z\u0080-\u024F]{2,}(?:. |-| |'))*[a-zA-Z\u0080-\u024F]{2,}$"))
                            if (Regex.IsMatch(enumerator.Current.DataRow.ItemArray[i].ToString(), @"^([a-zA-Z\u0080-\u024F]{2,}(?:. |-| |'))*[a-zA-Z\u0080-\u024F]{2,}$"))
                            {
                                //check Geonames for the name, if it exists, use that
                                name = enumerator.Current.DataRow.ItemArray[i].ToString();

                                return name;

                            }
                        }
                    }
                }
                else
                {
                    if (item.Contains(point))
                    {

                        string name = "No Name";
                        //for (int i = 0; i < item.DataRow.ItemArray.Length; i++){
                        for (int i = 0; i < enumerator.Current.DataRow.ItemArray.Length; i++)
                        {
                            //if (Regex.IsMatch(item.DataRow.ItemArray[i].ToString(), @"^([a-zA-Z\u0080-\u024F]{2,}(?:. |-| |'))*[a-zA-Z\u0080-\u024F]{2,}$"))
                            if (Regex.IsMatch(enumerator.Current.DataRow.ItemArray[i].ToString(), @"^([a-zA-Z\u0080-\u024F]{2,}(?:. |-| |'))*[a-zA-Z\u0080-\u024F]{2,}$"))
                            {
                                //check Geonames for the name, if it exists, use that
                                name = enumerator.Current.DataRow.ItemArray[i].ToString();

                                return name;

                            }
                        }
                        return name;
                    }
                }
            }
            return "Not Found";
        }
        public override string GetFilepath()
        {
            return this.filepath;
        }

        override public bool checkScrapeColumn(double lat, double lng, string[] values)
        {
            Coordinate coord = new Coordinate(lng, lat);
            Point point = new Point(coord);

            foreach (string name in values)
            {
                 //var reducedShapes = featureSet.Features.Where(current => current.DataRow.ItemArray..Contains(name, StringComparer.OrdinalIgnoreCase));
                var reducedShapes = featureSet.Features;
                foreach (var shape in reducedShapes)
                {
                    for (int i = 0; i < shape.DataRow.ItemArray.Length; i++)
                    {
                       //  Console.WriteLine(shape.DataRow.ItemArray[i].ToString().ToLower());

                        if (shape.DataRow.ItemArray[i].ToString().ToLower() == name)
                        {
                            Console.WriteLine(shape.DataRow.ItemArray[i].ToString().ToLower() + "==" + name);
                            Polygon poly = shape.BasicGeometry as Polygon;
                            if (poly == null)
                            {
                                MultiPolygon poly2 = shape.BasicGeometry as MultiPolygon;
                                if (poly2.Contains(point))
                                {
                                    return true;
                                }
                            }
                            else
                            {
                                if (poly.Contains(point))
                                {
                                    return true;
                                }
                            }
                        }

                    }

                }
            }
            return false;
        }
    }
}
