﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Xml;
using System.Xml.Linq;
using SharpKml.Engine;
using SharpKml.Dom;
using SharpKml.Base;
using static DotSpatial.Topology.GeometryCollection;

namespace coordinateFileVerification
{

    //Possible class to use to sort shapes by east to west, possibly improving efficiency by skipping shapes less than given longitude;
    public class EastWestBounds : IComparable
    {
        public double eastBound;
        public double westBound;

        public EastWestBounds(double east, double west)
        {
            eastBound = east;
            westBound = west;
        }

        public int CompareTo(Object obj)
        {
            EastWestBounds ewb = (EastWestBounds)obj;
            return (int)this.westBound - (int)ewb.westBound;
        }

        public override string ToString()
        {
            return eastBound + "--" + westBound;
        }
    }
    public class EastWestBoundsComparer : IComparer<EastWestBounds>
    {
        public int Compare(EastWestBounds x, EastWestBounds ewb)
        {
            if (x.westBound < ewb.westBound)
            {
                return -1;

            }
            else if (x.westBound > ewb.westBound)
            {
                return 1;
            }
            //if equal check eastern boundry
            else
            {
                if (x.eastBound < ewb.eastBound)
                {
                    return -1;
                }
                else if (x.eastBound == ewb.eastBound)
                {
                    return 1;
                }
                else
                {
                    return 1;
                }
            }
        }
    }

    //KML class for comparison
    public class KMLFile : GeoFile
    {
        //create a list sorted by west boundry, you can check if lat is more west than that coordinate, and if so, it can be eliminated 
        SortedList<EastWestBounds, Polygon> Polygons;
        String filepath;
        String comparison;
        double buffer;
        


        public KMLFile(String filepath, String comparison)
        {
            this.filepath = filepath;
            this.comparison = comparison;
            this.buffer = 0.0;
            Polygons = new SortedList<EastWestBounds, Polygon>(new EastWestBoundsComparer());
            Console.WriteLine("Contruct KML File:" + filepath);

            TextReader reader = new StreamReader(filepath);
            KmlFile file = KmlFile.Load(reader);

            Kml kml = file.Root as Kml;
            foreach (var item in kml.Flatten().OfType<Placemark>())
            {
                //Console.WriteLine(item.Name);
                foreach (Polygon polygon in item.Flatten().OfType<SharpKml.Dom.Polygon>())
                {
                    Polygons.Add(new EastWestBounds(polygon.CalculateBounds().East, polygon.CalculateBounds().West), polygon);
                }
            }
        }
        public KMLFile(String filepath, String comparison, double buffer)
        {
            this.filepath = filepath;
            this.comparison = comparison;
            this.buffer = buffer;
            Polygons = new SortedList<EastWestBounds, Polygon>(new EastWestBoundsComparer());
            Console.WriteLine("Contruct KML File:" + filepath);

            TextReader reader = new StreamReader(filepath);
            KmlFile file = KmlFile.Load(reader);

            Kml kml = file.Root as Kml;
            foreach (var item in kml.Flatten().OfType<Placemark>())
            {
                //Console.WriteLine(item.Name);
                foreach (Polygon polygon in item.Flatten().OfType<SharpKml.Dom.Polygon>())
                {
                    Polygons.Add(new EastWestBounds(polygon.CalculateBounds().East, polygon.CalculateBounds().West), polygon);
                }
            }
        }


        public override void ReadPolygons()
        {


        }
        public override string GetComparisonColumn()
        {
            return comparison;
        }

        public bool checkPolygonForPoint(SharpKml.Dom.Polygon poly, double latitude, double longitude)
        {
            SharpKml.Base.Vector coordinates = new SharpKml.Base.Vector();
            coordinates.Latitude = latitude;
            coordinates.Longitude = longitude;



            Vector endPoint = poly.OuterBoundary.LinearRing.Coordinates.Last<Vector>();
            double lastLng = endPoint.Longitude;
            double lastLat = endPoint.Latitude;
            bool isInside = false;



            if (!poly.CalculateBounds().Contains(coordinates.Latitude, coordinates.Longitude))
            {
                return false;
            }
            int coordinateCount = poly.OuterBoundary.LinearRing.Coordinates.Count;
            double startLng;
            double startLat;
            for (int i = 0; i < coordinateCount; i++)
            {

                startLat = lastLat;
                startLng = lastLng;
                endPoint = poly.OuterBoundary.LinearRing.Coordinates.ElementAt(i);

                lastLat = endPoint.Latitude;
                lastLng = endPoint.Longitude;

                isInside ^= (lastLat > coordinates.Latitude ^ startLat > coordinates.Latitude) &&
                    ((coordinates.Longitude - lastLng) < (((coordinates.Latitude - lastLat) * (startLng - lastLng)) / (startLat - lastLat)));
            }

            return isInside;

        }

        override public bool checkScrapeColumn(double lat, double lng, string[] values)
        {
            SharpKml.Dom.Polygon[] polygons = Polygons.Values.ToArray();
            foreach (string name in values)
            {

                foreach (Polygon poly in polygons)
                {

                    if (poly.GetParent<Placemark>().Name.Replace("<at>", "").Replace("<openparen>", "").Replace("<closeparen>", "").ToLower().Replace(" ", "") == name.ToLower().Replace(" ", ""))
                    {
                        if (checkPolygonForPoint(poly, lat, lng))
                        {

                            return true;
                        }
                        else
                        {
                            Console.WriteLine(name);
                            Console.WriteLine(poly.GetParent<Placemark>().Name.Replace("<at>", "").Replace("<openparen>", "").Replace("<closeparen>", "").Replace(" ", "").ToLower().Replace(" ", "")) ;
                            Console.WriteLine("++++++");
                            //return false;
                        }
                    }

                }
            }
            return false;
        }

        override public string findContainingPolygon(double latitude, double longitude)
        {
            SharpKml.Dom.Polygon[] polygons = Polygons.Values.ToArray();


            for (int i = 0; i < polygons.Length; i++)
            {

                if (checkPolygonForPoint(polygons[i], latitude, longitude))
                {
                    Placemark poly = polygons[i].GetParent<Placemark>();

                    return poly.Name.Replace("<at>", "").Replace("<openparen>", "").Replace("<closeparen>", "");
                }
            }
            return "Not Found";
        }
        public override string GetFilepath()
        {
            return this.filepath;
        }
        
    }
}

