﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Diagnostics;
using System.ComponentModel;
using System.Text;
using MySql.Data.MySqlClient;
using Json;
using System.Text.RegularExpressions;
using System.Net;
using System.Xml;
using System.Xml.Linq;

namespace coordinateFileVerification
{
    public class AggdataList
    {
        public int id;
        public string name;
        public int payloadId;
        public int size;
        public int parentId;
        private List<Location> Locations;
        public List<string> columnsUsed;


        public AggdataList(int id, string name, int payload, int size)
        {
            this.id = id;
            this.name = name;
            payloadId = payload;
            this.size = size;
            Locations = new List<Location>();
        }
        public Location AddLocation(int id, double lat, double lng,AggdataList list)
        {
            Location location = new Location(id, lat, lng,list);

            Locations.Add(location);
            return location;
        }
        public Location GetLocation(int i)
        {
            return Locations.ElementAt(i);
        }
        public List<AggdataList.Location> GetLocations()
        {
            return Locations;
        }
        //inner class for locations
        public class Location
        {
            public int id;
            public double lat;
            public double lng;
            public Dictionary<string, string> scrapedInfo;
            public AggdataList list;

            public Location(int id, double lat, double lng,AggdataList outerList)
            {
                this.id = id;
                this.lat = lat;
                this.lng = lng;
                scrapedInfo = new Dictionary<string, string>();
                this.list = outerList;

            }
            public void appendScrapeInfo(string columnName, string value)
            {
                if (!scrapedInfo.ContainsKey(columnName))
                {
                   // Console.WriteLine(value);
                    scrapedInfo.Add(columnName, value);
                }

            }
        }
    }

    public class CountryInfo {
        public string altName;
        public Dictionary<string, string> states;
        public CountryInfo(string name)
        {
            altName = name;
            states = new Dictionary<string, string>();
        }
    }

    public class runCoordinatesCheck
    {
        private List<GeoFile> files;
        private List<AggdataList> Lists;
        private Dictionary<string, CountryInfo> fullNames;


        public runCoordinatesCheck(string[] inputFiles, string[] comparisonColumn, string[] sqlConditions, double[] buffers, int[][] listIds)
        {
            files = new List<GeoFile>();
            int fileIndex = 0;

            //Loads up all the files beforehand

            Console.WriteLine(inputFiles.Length);
            foreach (string url in inputFiles)
            {

                foreach (string shape in Directory.GetFiles(url))
                {

                    //check if filepath ends in kml or shp, else returns error and Add Shapefile to files to check
                    if (Path.GetExtension(shape).Equals(".kml"))
                    {
                        Console.WriteLine("Processing " + shape + " as KML file");
                        if (buffers[fileIndex] != 0.0)
                        {
                            files.Add(new KMLFile(shape, comparisonColumn[fileIndex], buffers[fileIndex]));
                            files[fileIndex].setFileIndex(fileIndex);
                            files[fileIndex].setCondition(sqlConditions[fileIndex]);
                            files[fileIndex].setListIds(listIds[fileIndex]);

                        }
                        else
                        {
                            files.Add(new KMLFile(shape, comparisonColumn[fileIndex]));
                            files[fileIndex].setFileIndex(fileIndex);

                            files[fileIndex].setCondition(sqlConditions[fileIndex]);
                            files[fileIndex].setListIds(listIds[fileIndex]);
                        }

                    }
                    else if (Path.GetExtension(shape).Equals(".shp"))
                    {
                        Console.WriteLine("Processing " + shape + " as Shape file");
                        if (buffers[fileIndex] != 0.0)
                        {

                            files.Add(new ShapeFile(shape, comparisonColumn[fileIndex], buffers[fileIndex]));
                            files[fileIndex].setFileIndex(fileIndex);
                            files[fileIndex].setCondition(sqlConditions[fileIndex]);
                            if (listIds[fileIndex] != null)
                            {
                                files[fileIndex].setListIds(listIds[fileIndex]);
                            }
                            else
                            {
                                int[] defaultId = { 0 };
                                files[fileIndex].setListIds(defaultId);
                            }


                        }
                        else
                        {

                            files.Add(new ShapeFile(shape, comparisonColumn[fileIndex]));
                            files[fileIndex].setFileIndex(fileIndex);
                            files[fileIndex].setCondition(sqlConditions[fileIndex]);
                            files[fileIndex].setListIds(listIds[fileIndex]);
                        }


                    }


                }

                fileIndex++;
            }
            //End file loading
            Console.WriteLine("All Files Loaded Length ={0}", files.ToArray().Length);


            //Get List names
            Lists = new List<AggdataList>();
            MySqlConnection connection = getDBConnection();
            connection.Open();
            //returns the name of each list
            string Sqlcode = "SELECT lists.id,short_name,payloads.id,payloads.archived_location_count,parent_id FROM lists join payloads ON lists.id=payloads.list_id AND payloads.id IN " +
                "(Select pl.id "
                + "  From payloads pl Join (Select _pl.list_id As list_id, "
                + "     Max(_pl.scraped_at) As scraped_at "
                + "    From payloads _pl "
                + "    Where _pl.status = 'published' "
                + "    Group By _pl.list_id) x On pl.list_id = x.list_id And pl.scraped_at = "
                + "      x.scraped_at "
                + "  Where pl.status = 'published') ORDER BY lists.id LIMIT 100";

            MySqlCommand query = new MySqlCommand(Sqlcode, connection);
            query.CommandTimeout = 0;
            MySqlDataReader reader = query.ExecuteReader();
            while (reader.Read())
            {
                //adds List info to lists
                AggdataList newList = new AggdataList((int)reader.GetValue(0), (string)reader.GetValue(1), (int)reader.GetValue(2), (int)reader.GetValue(3));
                if (reader.IsDBNull(4))
                {
                    newList.parentId = (int)reader.GetValue(0);
                }
                else
                {
                    newList.parentId = (int)reader.GetValue(4);
                }
                Lists.Add(newList);

            }

            reader.Close();

            //Adds Locations to each list and eliminates locations that fail a basic evaluation test
            //Add Headers to Output CSV
            File.WriteAllText("C:\\Users\\Public\\Downloads\\AggdataVerifiedLocations.csv",
                " UID,"
                + "File_Name,"
                + "List_Name,"
                + "List_Size,"
                + "List_Id,"
                + "Parent_Id,"
                + "Archived_Location_Id,"
                + "Latitude,"
                + "Longitude,"
                + "ScrapeInfo,"
                + "Comparison,"
                + "ScrapeData,"
                + "FileResult,"
                + "IsUnknown"
                + ",\n");
            int UID = 0;
            connection.Close();
            foreach (AggdataList list in Lists)
            {
                string Sqlselect = "SELECT id,latitude,longitude,country,state,city,zip_code,county,address,address_line_2,address_line_3";

                foreach (string key in comparisonColumn)
                {
                    if (Sqlselect.Remove(0, 7).Contains(key))
                    {

                        continue;
                    }
                    Console.WriteLine(key);
                    Sqlselect += "," + key;

                }
                Sqlselect += " from archived_locations WHERE payload_id=" + list.payloadId + " AND list_id=" + list.id;
                //               Console.WriteLine(Sqlselect);

                connection.Open();
                query = new MySqlCommand(Sqlselect, connection);
                query.CommandTimeout = 0;

                reader = query.ExecuteReader();
                //adding Locations to List

                while (reader.Read())
                {

                    //if(Data is not in Land Shapefile)
                    //Return that location country is unfound and break to next location

                    try
                    {
                        AggdataList.Location listLocation = list.AddLocation(
                           (int)reader.GetValue(0),
                           (double)reader.GetValue(1),
                           (double)reader.GetValue(2),
                           list
                        );
                        for (int i = 3; i < reader.FieldCount; i++)
                        {
                            //gets every extra column after id,lat,lng for comparisons                          

                            string columnValue;
                            if (reader.GetValue(i) == System.DBNull.Value)
                            {
                                columnValue = "";
                            }
                            else
                            {
                                columnValue = (string)reader.GetValue(i);
                            }
                            //Console.WriteLine(reader.GetName(i)+":"+columnValue);
                            listLocation.appendScrapeInfo(reader.GetName(i), columnValue.ToLower());


                        }
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e.Message);
                        continue;
                    }



                }//Get Locations
                connection.Close();

                Console.WriteLine("_________________________");



                //For each file
                //if the current process is a child process, run the test
                //else create a new child process to run the test on the file

                foreach (GeoFile file in files.ToArray())
                {
                    Process currentProcess = Process.GetCurrentProcess();
                    if (getParentProcess(currentProcess.Id) == null)
                    {
                        //this is the root process, create child process to run test on this file
                        Console.WriteLine("Creating new process");
                    }
                    else
                    {
                        //run the test on the file.
                        Console.WriteLine("process {0} running test on {1}", currentProcess.Id, file.filepath);
                    }
                }
                /*  foreach (AggdataList.Location location in list.GetLocations())
                  {





                      if (fullNames == null)
                      {
                          fullNames = new Dictionary<string, CountryInfo>(); ;

                      }


                      //getAlternateNames(location.scrapedInfo["country"].ToLower(), location.scrapedInfo["state"].ToLower());
                      string[] locationNames = new string[2] ;
                      Console.WriteLine(files.ToArray().Length);
                      foreach (GeoFile file in files.ToArray())
                      {
                          if (file.fileIndex >= 1)
                          {
                              Console.WriteLine("next file");
                          }
                          Console.WriteLine(list.name);
                          Console.WriteLine("File #:{0} condition:{1} url={2}",file.fileIndex,file.condition,file.GetFilepath());
                          if (location.scrapedInfo["state"] == "wa" && file.condition.Split('=')[0].ToLower()=="state")
                          {
                              Console.WriteLine("stop");
                          }
                          if (!inLocationsToBeChecked(location,file))
                          {

                              Console.WriteLine("Location returned {0}, skipping location", inLocationsToBeChecked(location, file));

                              continue;
                          }
                          if (file.GetComparisonColumn() == "country")
                          {

                              if (location.scrapedInfo["country"] == "")
                              {
                                  continue;
                              }

                              getAlternateNames(location.scrapedInfo["country"].ToLower(), location.scrapedInfo["state"].ToLower());

                              locationNames[0] = location.scrapedInfo["country"].ToLower();
                              Console.WriteLine("Scraped Data:"+location.scrapedInfo["country"].ToLower());
                              if (!fullNames.ContainsKey(location.scrapedInfo["country"].ToLower()))
                              {
                                  continue;
                              }
                              Console.WriteLine(fullNames[location.scrapedInfo["country"].ToLower()].altName);
                              locationNames[1]=fullNames[location.scrapedInfo["country"].ToLower()].altName;
                          }else if(file.GetComparisonColumn() == "state")
                          {

                              locationNames[0] = location.scrapedInfo["state"].ToLower();
                              if (location.scrapedInfo["country"] == "")
                              {
                                  continue;
                              }
                              if (location.scrapedInfo["state"] == "")
                              {
                                  continue;
                              }
                              getAlternateNames(location.scrapedInfo["country"].ToLower(), location.scrapedInfo["state"].ToLower());
                              Console.WriteLine(location.scrapedInfo["country"].ToLower());
                              Console.WriteLine(location.scrapedInfo["state"].ToLower());
                              if (!fullNames.ContainsKey(location.scrapedInfo["country"].ToLower()))
                              {
                                  continue;
                              }
                              Console.WriteLine(fullNames[location.scrapedInfo["country"].ToLower()].altName);
                              Console.WriteLine(fullNames[location.scrapedInfo["country"].ToLower()].states.ContainsKey(location.scrapedInfo["state"].ToLower()));
                              if (fullNames[location.scrapedInfo["country"].ToLower()].states.ContainsKey(location.scrapedInfo["state"].ToLower()))
                              {
                                  locationNames[1] = fullNames[location.scrapedInfo["country"].ToLower()].states[location.scrapedInfo["state"]];
                              }
                              else
                              {
                                  continue;
                                  locationNames[1] = fullNames[location.scrapedInfo["country"]].states[location.scrapedInfo["state"]];
                              }

                          }
                          else
                          {
                               locationNames[0] = location.scrapedInfo[file.GetComparisonColumn()];
                                Array.Resize(ref locationNames, 1);
                          }

                          file.ReadPolygons();

                         //check if polygon exists with that name and see if the coords are inside
                          if (file.checkScrapeColumn(location.lat,location.lng,locationNames))
                          {
                              Console.WriteLine("Shape found and was a match");
                          }
                          else
                          {
                              Console.WriteLine("Shape was not correct, getting true shape");

                              string verifiedLocation = file.findContainingPolygon(location.lat, location.lng);

                              int isUnknown = 0;
                              if(verifiedLocation=="Not Found")
                              {
                                  if((location.scrapedInfo["country"]!="united states" || location.scrapedInfo["country"] != "us")&&file.GetComparisonColumn()=="state")
                                  {
                                      continue;
                                  }
                                  isUnknown = 1;
                              }

                             File.AppendAllText("C:\\Users\\Public\\Downloads\\AggdataVerifiedLocations.csv", UID
                          + "," + Path.GetFileName(file.GetFilepath())
                          + "," + "\""+list.name+"\""
                          + "," + list.size
                          + "," + list.id
                          + "," + list.parentId
                          + "," + location.id
                          + "," + location.lat
                          + "," + location.lng
                          + "," + "\""+location.scrapedInfo["city"]+ "," + location.scrapedInfo["state"]
                          + "," + location.scrapedInfo["country"]
                         + "," + location.scrapedInfo["zip_code"]
                          + "," + location.scrapedInfo["county"]
                         + "," + location.scrapedInfo["address"]
                          + "," + location.scrapedInfo["address_line_2"]
                          + "," + location.scrapedInfo["address_line_3"] + "\""
                          + "," + file.GetComparisonColumn()
                          + "," + "\"" + location.scrapedInfo[file.GetComparisonColumn()] + "\""
                          + "," + "\"" + verifiedLocation + "\""
                          + "," + isUnknown
                          + ",\n"
                          );
                              UID++;
                          }


                      }

                      Console.WriteLine("===========================================");
                  }
                  reader.Close();


              }*/



                Console.WriteLine("Finished!");
            }
        }

        /*******************************************************************************************************************/

        private Process getParentProcess(int id)
        {

        }

        private bool inLocationsToBeChecked(AggdataList.Location location,GeoFile file)
        {
            if (file.condition.Split('=')[0].ToLower()=="")
            {
                return true;
            }
            Console.WriteLine(location.scrapedInfo[file.condition.Split('=')[0].ToLower()] + " != " + file.condition.Split('=')[1].ToLower());
            if (location.scrapedInfo[file.condition.Split('=')[0].ToLower()] != file.condition.Split('=')[1].ToLower())
            {
                Console.WriteLine("Error 0");
                return false;
            }else if (!file.listIds.Contains(location.list.id) && !file.listIds.Contains(0))
            {
                Console.WriteLine("Error 1");
                return false;
            }else if (location.scrapedInfo[file.GetComparisonColumn()]=="")
            {
                Console.WriteLine("Error 2");
                return false;
            }
            else
            {
                return true;
            }
             
        }
        private MySqlConnection getDBConnection()
        {
            string server = "104.236.59.125";
            string database = "premium";
            string user = "cyfe";
            string pass = "1stSourceC";
            string connectionstring;

            connectionstring =
            "server=" + server + ";" +
            "database=" + database + ";" +
            "user=" + user + ";" +
            "password=" + pass + ";";

            MySqlConnection connection = null;

            try
            {
                return connection = new MySqlConnection(connectionstring);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return null;
            }
        }

        private string [] getAlternateNames(string country,string state)
        {
            Console.WriteLine("Getting Alternate names for " + country.ToLower());
            if (country == "")
            {
                Console.WriteLine("Getting Alternate names for " + state.ToLower());
                return new string[] { state.ToLower() };
            }else if(state==""){
                Console.WriteLine("Getting Alternate names for " + state);
                if (!fullNames.ContainsKey(country))
                {
                    fullNames.Add(country, new CountryInfo(country));
                }
                
                return new string[] { country };
            }
            //check if fullNames has existing info on State and Country Abbreviation
            if (fullNames.ContainsKey(country.ToLower()))
            {
                //check if there is info on that state
                Console.WriteLine("Country Stored..Pulling Values for " + country + " and " + state);
                if (fullNames[country].states.ContainsKey(state.Replace(" ","")))
                {
                    Console.WriteLine("State Found");
                    Console.WriteLine(country+":"+fullNames[country.ToLower()].altName + ":" + state + ":" + fullNames[country.ToLower()].states[state.Replace(" ", "")]);

                }
                else
                {

                    Console.WriteLine("Country doesnt have state");
                    return new string [] { country,state,fullNames[country].altName };
                }
                if (!fullNames[country.ToLower()].states.ContainsKey(state.ToLower()))
                {
                    Console.WriteLine("State is unofficial or doesn't exist");
                    return new string[] { country.ToLower(), fullNames[country.ToLower()].altName, state.ToLower() };
                }
                return new string[] { country.ToLower(), fullNames[country.ToLower()].altName, state.ToLower(), fullNames[country.ToLower()].states[state.ToLower()] };
            }
            else{
                //get info from geonames API
                Console.WriteLine("Pulling GeonameInfo");
               
                //check response is status message for overuse

                //check if results are more than 0, else get infomation on that country name
                XDocument xml = XDocument.Parse(getGeonames(country.ToLower()));
                if (xml.Descendants("geoname").Count() == 0)
                {
                    //Check if name is not abbreviation, request information on that country name and get abbreviation
                    Console.WriteLine("Country Info may not in abbreviated form, getting abbreviation");
                    string countryURL = "http://api.geonames.org/search?username=shanet&featureCode=PCLD&featureCode=PCLI&orderBy=relevance&name_equals=" + WebUtility.UrlEncode(country);
                   
                    HttpWebRequest request2 = WebRequest.Create(countryURL) as HttpWebRequest;
                    System.Net.ServicePointManager.ServerCertificateValidationCallback = (senderX, certificate, chain, sslPolicyErrors) => { return true; };
                    HttpWebResponse response2 = request2.GetResponse() as HttpWebResponse;
                    response2.Headers["ContentType"] = "text/xml";
                    Stream stream2 = response2.GetResponseStream();
                    
                    StreamReader reader2 = new StreamReader(stream2, Encoding.UTF8);
                    string xmlstring2 = reader2.ReadToEnd();
                    String preamble = Encoding.UTF8.GetString(Encoding.UTF8.GetPreamble());
                    if (xmlstring2.StartsWith(preamble))
                    {
                        var lastIndexOfUtf8 = preamble.Length - 1;
                        xmlstring2 = xmlstring2.Remove(0, lastIndexOfUtf8);
                    }
                    
                    XDocument xml2 = XDocument.Parse(xmlstring2);
                    //if result is still empty, return only given name and nothing else can be done
                    if (xml2.Descendants("geoname").Count() == 0)
                    {
                        Console.WriteLine("Couldn't find supplimental information");
                        return new string[] { country.ToLower(), state.ToLower() };
                    }
                    XElement node = xml2.Descendants("geoname").First();
                    Console.WriteLine("Country Code:" + node.Descendants("countryCode").First().Value.ToLower());

                    //recursively call with the new country code
                    xml= XDocument.Parse(getGeonames(node.Descendants("countryCode").First().Value.ToLower()));
                    //make a 2nd entry with extended name
                    Console.WriteLine("Key " + country.ToLower());
                    if (!fullNames.ContainsKey(country.ToLower()))
                    {
                        Console.WriteLine("Key " + country.ToLower() + " does not exist, adding to table");
                        fullNames.Add(country.ToLower(), new CountryInfo(node.Descendants("countryCode").First().Value.ToLower()));
                        
                    }
                    fullNames[country.ToLower()].altName = node.Descendants("countryCode").First().Value.ToLower();
                    foreach (XElement stateInfo in xml.Descendants("geoname"))
                    {
                        string stateabbr;
                        if (stateInfo.Descendants("adminCode1").First().Attribute("ISO3166-2") != null)
                        {
                            stateabbr = stateInfo.Descendants("adminCode1").First().Attribute("ISO3166-2").Value.ToLower();
                        }
                        else
                        {
                            stateabbr = stateInfo.Descendants("adminCode1").First().Value.ToLower();
                        }
                        Console.WriteLine("Country Info:" + stateInfo.Descendants("countryName").First().Value.ToLower());
                        Console.WriteLine("Country Code:" + stateInfo.Descendants("countryCode").First().Value.ToLower());
                        Console.WriteLine("State Abbrev:" + stateabbr.ToLower());
                        Console.WriteLine("State Name:" + stateInfo.Descendants("adminName1").First().Value.ToLower());
                        fullNames[country.ToLower()].states.Add(stateabbr, stateInfo.Descendants("adminName1").First().Value.ToLower());
                        if (!fullNames[country.ToLower()].states.ContainsKey(stateInfo.Descendants("adminName1").First().Value.ToLower()))
                        {
                            fullNames[country.ToLower()].states.Add(stateInfo.Descendants("adminName1").First().Value.ToLower().Replace(" ", ""), stateabbr.ToLower());
                        }
                        
                    }
                
            }
                //else parse info recieved

                foreach (XElement node in xml.Descendants("geoname"))
                {
                    string stateabbr;
                    if( node.Descendants("adminCode1").First().Attribute("ISO3166-2")!=null){
                         stateabbr = node.Descendants("adminCode1").First().Attribute("ISO3166-2").Value.ToLower();
                    }
                    else
                    {
                        stateabbr= node.Descendants("adminCode1").First().Value.ToLower();
                    }
                    Console.WriteLine("Country Info:" + node.Descendants("countryName").First().Value.ToLower());
                    Console.WriteLine("Country Code:" + node.Descendants("countryCode").First().Value.ToLower());
                    Console.WriteLine("State Abbrev:" + stateabbr);
                    Console.WriteLine("State Name:" + node.Descendants("adminName1").First().Value.ToLower());

                    
                    if (!fullNames.ContainsKey(node.Descendants("countryCode").First().Value.ToLower()))
                    {
                        fullNames.Add(node.Descendants("countryCode").First().Value.ToLower(), new CountryInfo(node.Descendants("countryName").First().Value.ToLower()));
                    }
                    if (!fullNames[node.Descendants("countryCode").First().Value.ToLower()].states.ContainsKey(stateabbr))
                    {
                        fullNames[node.Descendants("countryCode").First().Value.ToLower()].states.Add(stateabbr, node.Descendants("adminName1").First().Value.ToLower());
                    }
                    fullNames[node.Descendants("countryCode").First().Value.ToLower()].altName = node.Descendants("countryName").First().Value.ToLower();
                    
                }
                if (!fullNames[country.ToLower()].states.ContainsKey(state.ToLower()))
                {
                    Console.WriteLine("State is unofficial or doesn't exist");
                    return new string[] { country.ToLower(), fullNames[country.ToLower()].altName, state.ToLower() };
                }
                return new string[] { country.ToLower(), fullNames[country.ToLower()].altName, state.ToLower(), fullNames[country.ToLower()].states[state.ToLower()] };
            }
           
        }

        public string getGeonames(string countryAbbrev)
        {
            string apiURL = "http://api.geonames.org/search?username=shanet&style=full&countryBias=US&featureCode=ADM1&country=" + countryAbbrev;

            HttpWebRequest request = WebRequest.Create(apiURL) as HttpWebRequest;
            System.Net.ServicePointManager.ServerCertificateValidationCallback = (senderX, certificate, chain, sslPolicyErrors) => { return true; };
            HttpWebResponse response = request.GetResponse() as HttpWebResponse;
            response.Headers["ContentType"] = "text/xml";
            Stream stream = response.GetResponseStream();
            String preamble = Encoding.UTF8.GetString(Encoding.UTF8.GetPreamble());
            StreamReader reader = new StreamReader(stream, Encoding.UTF8);
            string xmlstring = reader.ReadToEnd();
            if (xmlstring.StartsWith(preamble))
            {
                var lastIndexOfUtf8 = preamble.Length - 1;
                xmlstring = xmlstring.Remove(0, lastIndexOfUtf8);
            }
            return xmlstring;
        }

    }
}
