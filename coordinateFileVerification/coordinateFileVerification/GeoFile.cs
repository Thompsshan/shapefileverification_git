﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;



namespace coordinateFileVerification
{
    public abstract class GeoFile
    {
        private Object [] Polygons;
        private double buffer;
        public int fileIndex;
        public string condition;
        public int[] listIds;


        
        abstract public void  ReadPolygons();
        abstract public string findContainingPolygon(double lat, double lng);
        abstract public string GetComparisonColumn();
        abstract public string GetFilepath();
        abstract public bool checkScrapeColumn(double lat, double lng, string[] scrapedInfo);
        public void setFileIndex(int index)
        {
            this.fileIndex = index;
        }
        public void setCondition(string cond)
        {
            this.condition = cond;
        }
        public void setListIds(int [] ids)
        {
            listIds = (int[])ids.Clone();
        }
    }
}
