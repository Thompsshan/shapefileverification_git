﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace coordinateFileVerification
{
    public class studyCoordinates
    {
        public int LocationID;
        public double latitude;
        public double longitude;
        

        public studyCoordinates(int locationId, double Latitude,double Longitude)
        {
            latitude = Latitude;
            longitude = Longitude;
            LocationID = locationId;
            
        }

        public static studyCoordinates createFromCSV(String[] tokens)
        {
            int studyID = Int32.Parse(tokens[0]);
            double latitude = Double.Parse(tokens[1]);
            double longitude = Double.Parse(tokens[2]);
            return new studyCoordinates(studyID, latitude, longitude);
        }
        //Allows user to pass in custom query
        public static studyCoordinates createFromDB(String SqlQuery)
        {
            return null;
        }
        //Default which pulls all locations from most recent versions of all lists
        public static studyCoordinates createFromDB()
        {
            return null;
        }
    }
}
