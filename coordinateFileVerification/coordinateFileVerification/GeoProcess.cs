﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace GeoProcess
{
    //Possibly use this class to create threads in the future
    class GeoProcess
    {
        public static String ProcessName
        {
            get
            {
                return GetProcess().ProcessName;
            }
        }

        public static GeoProcess ParentProcess
        {
            get
            {
                return GetParentProcess();
            }
        }

        public static String filename
        {
            get
            {
                return System.IO.Path.GetFileName(GetProcess().MainModule.FileName);
            }
        }
        
        public static String DirectoryName
        {
            get
            {
                return System.IO.Path.GetDirectoryName(GetProcess().MainModule.FileName);
            }
        }
        public static int ProcessId
        {
            get
            {
                return GetProcess().Id;
            }
        }

        private static GeoProcess GetParentProcess()
        {
            int ParentPid = 0;
            int Pid = GetProcess().Id;

            return null;
        
        }

        private static Process GetProcess()
        {
            return Process.GetCurrentProcess();
        }

    }
    
}
