﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using AggdataShapefileVerificationFramework;
using System.IO;
using System.Linq;
using System.Text;

namespace testExecutor
{
    class runTest
    {
        
        static void Main(string[] args)
        {
            
            
            
            

            Dictionary<string, CountryInfo> altNames = DataRetriever.DataStore.PlaceNames=new Dictionary<string, CountryInfo>();

            string outputFilepath = "C:\\Users\\Public\\Downloads\\AggdataVerifiedLocations.csv";
            string[] headers =
            {
                "UID",
                "Ali",
                "List_ID",
                "List",
                "List_Size",
                "List_Parent",
                "List_Scrape_Date",
                "Shapefile_Path",
                "Latitude",
                "Longitude",
                "Address",
                "Column",
                "Scrape_Data",
                "Test_Result",
                "Is_Unknown",
                "Verification_Score",
                "Geo_Accuracy",
                "country",
                "state",
                "zip_code",
                "county",
                "city"
            };
            AppendingCSVWriter output = new AppendingCSVWriter(outputFilepath, headers);

            DateTime date = DateTime.Today.AddDays(-1);
            
            
            List<AggdataList> lists = DataRetriever.getAggdataLists(date.ToString("yyyy-MM-dd"));            
        
            int listNumber = 0;

            Stopwatch runtime = new Stopwatch();
            runtime.Start();
            int locationNumber = 0;
            Console.WriteLine("Starting Test");
            TimeSpan end = new TimeSpan(7, 30, 0); //stop test at 7:30am
            foreach (AggdataList list in lists)
            {
                
                list.Locations = DataRetriever.getListLocations(list);
                
                if (list.Locations == null || list.Locations.Count==0)
                {
                    //if locations cant be retrieved, skip list
                    continue;
                }
                
                Console.Write(list.name+":");
                list.size = list.Locations.Count();
                listNumber++;
                AggdataList.Location prevLocation = list.Locations.First<AggdataList.Location>();
                DataRetriever.getAlternateLocationNames(prevLocation.country, prevLocation.state);

                if (altNames.ContainsKey("us"))
                {
                    if (altNames["us"].ChildPlaceNames.ContainsKey("pr") == false)
                    {
                        altNames["us"].ChildPlaceNames.Add("pr", new List<string>());
                        altNames["us"].ChildPlaceNames["pr"].Add("puerto rico");
                    }
                }
                //Console.Write("\r{0}:Clearing List from current Location Test File...",list.name);
                
                output.removeList(list.name);
               
                string currentDir = args[0];

                LinkedList<GeoFile> shapes = DataRetriever.getShapefiles(prevLocation,currentDir);
                Stopwatch listtime = new Stopwatch();
                listtime.Start();
                int tick = 0;
                
                foreach (AggdataList.Location location in list.Locations)
                {
                    
                    Console.Write("\r{0}:{1}",list.name, listtime.Elapsed.ToString(@"m\:ss"));

                    //Check if country has changed, if so get that countries names

                    if (location.country != prevLocation.country)
                    {
                        prevLocation = location;
                        shapes = DataRetriever.getShapefiles(prevLocation, currentDir);
                        DataRetriever.getAlternateLocationNames(prevLocation.country, prevLocation.state);              

                    }

                    if (location.state == null)
                    {
                        continue;
                    }
                    if (location.country == null)
                    {
                        continue;
                    }
                   
                    

                        //check if using an alternate name
                        string[] countryValues = { location.country.ToLower() };

                    if (altNames.ContainsKey(location.country.ToLower()))
                    {
                        countryValues = altNames[location.country.ToLower()].altNames.Concat(countryValues).ToArray();
                    }
                    else
                    {
                        //check if using an alternate name
                        Dictionary<string, CountryInfo>.Enumerator countryEnumerator = altNames.GetEnumerator();
                        while (countryEnumerator.MoveNext())
                        {
                            //if a Placename is found with an alternate name, set values to the current key
                            if (altNames[countryEnumerator.Current.Key].altNames.Contains(location.country.ToLower()))
                            {
                                countryValues[0] = countryEnumerator.Current.Key;
                                countryValues = countryValues.Concat(altNames[countryEnumerator.Current.Key].altNames).ToArray();

                                break;
                            }
                        }


                    }

                    string[] stateValues = { location.state.ToLower() };
                    if (!altNames.ContainsKey(countryValues[0]))
                    {
                        continue;
                    }
                    if (altNames[countryValues[0]].ChildPlaceNames.ContainsKey(location.state.ToLower()))
                    {
                        stateValues = stateValues.Concat(altNames[countryValues[0]].ChildPlaceNames[location.state.ToLower()]).ToArray();
                    }else{
                        Dictionary<string, List<string>>.Enumerator stateEnumerator = altNames[countryValues[0]].ChildPlaceNames.GetEnumerator();
                        while (stateEnumerator.MoveNext())
                        {
                            //if a Placename is found with an alternate name, set values to the current key
                            if (altNames[countryValues[0]].ChildPlaceNames[stateEnumerator.Current.Key].Contains(location.state.ToLower()))
                            {
                                stateValues[0] = stateEnumerator.Current.Key;
                                stateValues = altNames[countryValues[0]].ChildPlaceNames[stateEnumerator.Current.Key].Concat(stateValues).ToArray();
                            }
                        }
                        //Console.WriteLine("Couldn't find state alternate names, make sure location is either a proper name or ISO standard");
                    }




                    int verificationScore = 0;
                    string fileResult = "Not Found";
                    
                    foreach (GeoFile file in shapes)
                    {
                        //check country, then check highest res check (zip) and work up
                        fileResult = "Not Found";
                        string[] values;
                        if (file.comparison == "country")
                        {
                            values = countryValues;
                        }
                        else if (file.comparison == "state")
                        {
                            //file.outputReducedShapefile("bufferedFile");
                            values = stateValues;

                        }
                        else
                        {
                            values = new string[1];
                            values[0] = location.scrapedInfo[file.comparison];

                        }
                        if (location.scrapedInfo[file.comparison] == "")
                        {
                            verificationScore++;
                            continue;

                        }
                        if (file.checkScrapeColumn(location.lat, location.lng, values))
                        {
                           
                                verificationScore++;
                                fileResult = location.scrapedInfo[file.comparison];
                                continue;
                            
                            
                        }
                        else
                        {          


                            //Find Containing bounding boxes
                            string[] potentialShapes = file.findContainingBoundingBoxes(location.lat, location.lng);
                            
                            foreach (string shapeName in potentialShapes)
                            {
                                
                                GeoFile.Shape[] abstractShapes = file.GetPolygon(shapeName);
                                foreach (GeoFile.Shape shape in abstractShapes)
                                {
                                    if (file.checkPolygonForPoint(shape, location.lat, location.lng))
                                    {
                                        fileResult = file.getPolygonInfo(shape);
                                    }
                                }
                            }

                            //if lat long is "Not found" it could possibly be off the coast, so let it continue to see if it gets caught by a higher resolution file
                            int isUnknown = 0;
                            if (fileResult == "Not Found")
                            {
                                isUnknown = 1;
                            }
                            if (verificationScore >= 2 && location.scrapedInfo[file.comparison].Contains(fileResult))
                            {
                                continue;
                            }
                            

                            string[] outputRow = {
                                output.getUID().ToString(),
                                location.ali,
                                list.id.ToString(),
                                list.name,
                                list.size.ToString(),
                                list.parentId.ToString(),
                                list.published_date,
                                file.filepath,
                                location.lat.ToString(),
                                location.lng.ToString(),
                                "{" + location.address + "," + location.address_line_2 + "," + location.address_line_3 + "," + location.city + "," + location.county + "," + location.state + "," + location.country + "," + location.zip_code + "}",
                                file.GetComparisonColumn(),
                                location.scrapedInfo[file.GetComparisonColumn()],
                                fileResult,
                                isUnknown.ToString(),
                                verificationScore.ToString(),
                                location.geo_accuracy,
                                location.scrapedInfo["country"],
                                location.scrapedInfo["state"],
                                location.scrapedInfo["zip_code"],
                                location.scrapedInfo["county"],
                                location.scrapedInfo["city"]
                            };
                            output.Output(outputRow);
                            output.incrementUID();

                            //output
                            break;
                        }


                    }
                    
                    locationNumber++; 
                    if (DateTime.Now.TimeOfDay > end)
                    {
                        Console.WriteLine("\nTest Ending because of runtime");
                        Environment.Exit(1);
                    }
                }  
                   
                
                listtime.Stop();
                Console.Clear();
                Console.WriteLine("\r{0} completed in {1} ---Rate={2}", list.name, listtime.Elapsed.ToString(@"m\:ss"),(double)list.Locations.Count()/listtime.Elapsed.Seconds);

            }
            runtime.Stop();
            Console.WriteLine("Total runtime "+runtime.Elapsed.ToString(@"h\:m\:ss"));

        }

        

        private class CSVWriter : AggdataOutputWriter
        {
            int UID = 0;
            string fileName;
            public CSVWriter()
            {
                var encoding = Encoding.UTF8;
                fileName = "C:\\Users\\Public\\Downloads\\AggdataVerifiedLocations.csv";
                File.WriteAllText(fileName,
                    " UID,"
                    + "File_Name,"
                    + "List_Name,"
                    + "List_Size,"
                    + "List_Id,"
                    + "Parent_Id,"
                    + "Latitude,"
                    + "Longitude,"
                    + "ScrapeInfo,"
                    + "Comparison,"
                    + "ScrapeData,"
                    + "FileResult,"
                    + "IsUnknown,"
                    + "VerificationScore"
                    + ",\n", encoding);
            }
            public CSVWriter(string fileName, params string[] headers)
            {
                string headerText = "";
                foreach (string column in headers)
                {
                    headerText += column + ",";
                }
                headerText += "\n";
                var encoding = Encoding.UTF8;

                File.WriteAllText(fileName,
                    headerText, encoding);
                this.fileName = fileName;
            }
            public void Output(string[] OutputValues)
            {
                var encoding = Encoding.UTF8;
                int length = OutputValues.Length;
                int item = 0;

                while (item < length)
                {
                    File.AppendAllText(fileName, OutputValues[item] + ",", encoding);
                    item++;
                }
                File.AppendAllText(fileName, "\n", encoding);


            }

            public void Output(string OutputValue)
            {
                var encoding = Encoding.UTF8;
                File.AppendAllText(fileName, OutputValue, encoding);
            }

            public void incrementUID()
            {
                UID++;
            }
            public int getUID()
            {
                return UID;
            }
        }

        private class AppendingCSVWriter
        {
            int UID = 0;
            string fileName;
            
            public AppendingCSVWriter()
            {
                var encoding = Encoding.UTF8;
                fileName = "C:\\Users\\Public\\Downloads\\AggdataVerifiedLocations.csv";
                if(!File.Exists(fileName)){
                    string headerText = "";
                    File.WriteAllText(fileName,
                    "\"UID,\""
                    +"\"Ali\","
                    + "\"File_Name\","
                    + "\"List_Name\","
                    + "\"List_Size\","
                    + "\"List_Id\","
                    + "\"Parent_Id\","
                    + "\"Latitude\","
                    + "\"Longitude\","
                    + "\"ScrapeInfo\","
                    + "\"Comparison\","
                    + "\"ScrapeData\","
                    + "\"FileResult\","
                    + "\"IsUnknown\","
                    + "\"VerificationScore\""
                    + ",\n", encoding);
                                  
                }
                
            }
            public AppendingCSVWriter(string fileName, params string[] headers)
            {
                var encoding = Encoding.UTF8;
                if (!File.Exists(fileName))
                {
                    string headerText = "";
                    foreach (string column in headers)
                    {
                        headerText += "\""+column + "\""+ ",";
                    }
                    headerText += "\n";
                    

                    File.WriteAllText(fileName,headerText, encoding);
                }
                this.fileName = fileName;
            }

            public void Output(string[] OutputValues)
            {
                var encoding = Encoding.UTF8;
                int length = OutputValues.Length;
                int item = 0;

                
                    while (item < length)
                {
                    File.AppendAllText(fileName, "\"" + OutputValues[item]+ "\"" + ",", encoding);
                    item++;
                }
                File.AppendAllText(fileName, "\n", encoding);


            }

            public void Output(string OutputValue)
            {
                var encoding = Encoding.UTF8;
                File.AppendAllText(fileName, "\"+OutputValue\"", encoding);
            }

            public void removeList(string listName)
            {
                //remove all rows which come have a list with the given name
                var encoding = Encoding.UTF8;
                StreamReader fileStream = new StreamReader(fileName);
                string line;
                string contents="";
                while( (line=fileStream.ReadLine())!=null)
                {
                    if (line.Contains(listName))
                    {
                        continue;
                    }
                    else
                    {
                        contents += line+"\n";
                    }
                }
                fileStream.Close();
                File.WriteAllText(fileName,
                    "\"UID\","
                    + "\"Ali\","
                    + "\"File_Name\","
                    + "\"List_Name\","
                    + "\"List_Size\","
                    + "\"List_Id\","
                    + "\"Parent_Id\","
                    + "\"Latitude\","
                    + "\"Longitude\","
                    + "\"ScrapeInfo\","
                    + "\"Comparison\","
                    + "\"ScrapeData\","
                    + "\"FileResult\","
                    + "\"IsUnknown\","
                    + "\"VerificationScore\""
                    + ",\n", encoding);
                File.WriteAllText(fileName, contents, encoding);
            }
            public void incrementUID()
            {
                UID++;
            }
            public int getUID()
            {
                return UID;
            }
        }



    }

    
}
