﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using AggdataShapefileVerificationFramework;

using System.IO;
using System.Linq;
using System.Collections;

namespace testExecutor
{
    class testOptimizations
    {
        public static void Main()
        {
            Stopwatch watch = new Stopwatch();
            watch.Start();
            int totalFulltime = 0;
            int totalBoxtime = 0;
            int totalCanadatime = 0;
            KMLFile kml = (KMLFile)GeoFile.parseFileURL("E:\\CoordinateFileVerification\\coordinateFileVerification\\executeCoordinateVerifier\\GisFiles\\State\\cb_2016_us_state_500k.kml");
            watch.Stop();
            Console.WriteLine(watch.Elapsed.Seconds+ " seconds to construct cb_2016_us_state_500k.kml");
            totalFulltime += watch.Elapsed.Milliseconds;
            watch.Reset();

            watch.Start();
            KMLFile boxkml = (KMLFile)GeoFile.parseFileURL("E:\\CoordinateFileVerification\\coordinateFileVerification\\executeCoordinateVerifier\\GisFiles\\State\\Stateboxes.kml");
            watch.Stop();
            Console.WriteLine(watch.Elapsed.Seconds+ " seconds to construct cb_2016_us_state_500k.kml bounding boxes");
            totalBoxtime += watch.Elapsed.Milliseconds;
            watch.Reset();
            GeoFile.Shape [] shape =kml.GetPolygon("Washington");
            Console.WriteLine(shape.First().Polygon.OuterBoundary.LinearRing.Coordinates);
            

            watch.Start();
            ShapeFile canadaZip = (ShapeFile)GeoFile.parseFileURL("E:\\CoordinateFileVerification\\coordinateFileVerification\\executeCoordinateVerifier\\GisFiles\\Zipcode\\pshape\\CanadaPostalCodePolygons.shp");
            canadaZip.ReadPolygons();
            watch.Stop();
            Console.WriteLine(watch.Elapsed.Seconds + " seconds to construct canada postal shapefile");
            totalCanadatime += watch.Elapsed.Milliseconds;
            watch.Reset();
            GeoFile.Shape[] zip = canadaZip.GetPolygon("V1W4J6");
            Console.WriteLine(zip[0].Polygon);
            Console.WriteLine(shape.First().Polygon.OuterBoundary.LinearRing.Coordinates);
            List<AggdataList> aggdata = DataRetriever.getAggdataLists();







            foreach (AggdataList list in aggdata)
            {
                list.Locations=DataRetriever.getListLocations(list);
                if (list.Locations == null)
                {
                    continue;
                }
                foreach(AggdataList.Location location in list.Locations)
                {
                    watch.Start();
                    kml.findContainingPolygons(location.lat,location.lng);
                    watch.Stop();
                    Console.WriteLine("Full KML took {0} seconds",watch.Elapsed.Milliseconds);
                    totalFulltime += watch.Elapsed.Milliseconds;
                    watch.Reset();
                    watch.Start();
                    boxkml.findContainingPolygons(location.lat, location.lng);
                    watch.Stop();
                    totalBoxtime += watch.Elapsed.Milliseconds;
                    Console.WriteLine("Box KML took {0} seconds", watch.Elapsed.Milliseconds);
                                    
                   /* if(location.country.ToLower()=="canada" || location.country.ToLower() == "ca")
                    {*/
                        watch.Reset();
                        watch.Start();
                        canadaZip.findContainingPolygons(location.lat, location.lng);
                        watch.Stop();
                        totalCanadatime += watch.Elapsed.Milliseconds;
                        Console.WriteLine("canada took {0} seconds", watch.Elapsed.Milliseconds);
                        watch.Reset();
                    //}
                }
            }
            Console.WriteLine("Full:" + totalFulltime + " miliseconds");
            Console.WriteLine("Box:" + totalBoxtime + " miliseconds");
            Console.WriteLine("Box:" + totalCanadatime + " miliseconds");
        }

        
    }
}
